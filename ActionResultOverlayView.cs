using System;
using UIKit;
using CoreGraphics;
using Foundation;
using CoreAnimation;


namespace SimpleLibTouch
{
	public class ActionResultOverlayView : UIView
	{
		public ActionResultOverlayView(string imageName, string message) : base(new CGRect(0, 0, 320, 480))
		{
			var image = UIImage.FromBundle(imageName);
			_imageView = new UIImageView(image);
			_imageView.ContentMode = UIViewContentMode.Center;
			_imageView.Layer.CornerRadius = 8;
			_imageView.Layer.BorderColor = UIColor.White.CGColor;
			_imageView.Layer.BorderWidth = 3;
			AddSubview(_imageView);
			
			// Create the message label
			_label = new UILabel(new CGRect(0,0,0,0));
			_label.Text = message;
			_label.Opaque = false;
			_label.BackgroundColor = UIColor.Clear;
			_label.TextColor = UIColor.White;
			//_label.ShadowColor = UIColor.Black;
			//_label.ShadowOffset = new SizeF(2,2);
			_label.Font = UIFont.BoldSystemFontOfSize(16);
			_label.TextAlignment = UITextAlignment.Center;
			_label.Lines = 0;
			_label.LineBreakMode = UILineBreakMode.WordWrap;
			AddSubview(_label);

			if (Theme.Instance != null)
				Theme.Instance.ApplyTheme (this);

			// Measure label text
			var stringSize = message.StringSize(_label.Font, new CGSize(260, 100000), UILineBreakMode.WordWrap);
			
			const int margins = 10;
			
			var sizeImage = new CGSize(image.Size.Width + 20, image.Size.Height + 20);
			
			var totalWidth = NMath.Max(stringSize.Width, sizeImage.Width) + margins*2;
			var totalHeight = stringSize.Height + sizeImage.Height + margins * 3;
			
			_imageView.Frame = new CGRect((totalWidth - sizeImage.Width)/2, margins, sizeImage.Width, sizeImage.Height);
			_label.Frame = new CGRect(0, sizeImage.Height + margins * 2, totalWidth, stringSize.Height);
			
			// Setup self
			this.Alpha = 1.0f;
			this.BackgroundColor = UIColor.FromRGBA(0.3f, 0.3f, 0.3f, 0.9f);
			this.Layer.CornerRadius = 8;
			this.Frame = new CGRect(0, 0, totalWidth, totalHeight);

		}
		
		NSObject _observer;

		public UILabel Label
		{
			get 
			{
				return _label;
			}
		}
		
		public void Show()
		{
			_observer = NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.DidChangeStatusBarOrientationNotification, n=>
			{
				SetupOrientation();
			});
			
			UIApplication.SharedApplication.KeyWindow.AddSubview(this);
			SetupOrientation();

			_keepAlive = this;
			
			NSTimer.CreateScheduledTimer(1, delegate { Hide(); });
		}
		
		void CenterInParent()
		{
			var parent = this.Superview;
			this.Frame = new CGRect(
						(parent.Bounds.Width - this.Frame.Width)/2,
						(parent.Bounds.Height - this.Frame.Height)/2,
						this.Frame.Width,
						this.Frame.Height);
		}
		
		static object _keepAlive;
		
		[Export("Hide")]
		public void Hide()
		{
			UIView.BeginAnimations(null, IntPtr.Zero);
			UIView.SetAnimationDuration(1);
			this.Alpha = 0;
			UIView.SetAnimationDelegate(this);
			UIView.SetAnimationDidStopSelector(new ObjCRuntime.Selector("transitionDidStop:finished:context:"));
			UIView.CommitAnimations();

			NSNotificationCenter.DefaultCenter.RemoveObserver(_observer);
		}
		

		[Export("transitionDidStop:finished:context:")]
		void transitionDidStop(string animationID, NSNumber finished, IntPtr context)
		{
			this.RemoveFromSuperview();
			if (_keepAlive == this)
				_keepAlive = null;
		}

		void SetupOrientation()
		{
			var orientation = UIApplication.SharedApplication.StatusBarOrientation;

			float angle = 0;
			
			switch (orientation)
			{
				case UIInterfaceOrientation.LandscapeLeft:
					angle = (float)-Math.PI/2.0f;
					break;
					
				case UIInterfaceOrientation.LandscapeRight:
					angle = (float)Math.PI/2.0f;
					break;
					
				case UIInterfaceOrientation.Portrait:
		            angle = 0.0f;
					break;
					
				case UIInterfaceOrientation.PortraitUpsideDown:
					angle = (float)Math.PI;
					break;
			}
			
			this.Transform = CGAffineTransform.MakeRotation(angle);
			CenterInParent();
		}
		UILabel _label;
		UIImageView _imageView;
	}
}

