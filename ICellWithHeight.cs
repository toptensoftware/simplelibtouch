using System;
using UIKit;

namespace SimpleLibTouch
{
	public interface ICellWithHeight
	{
		nfloat MeasureHeight(UITableView tableView);
	}
}

