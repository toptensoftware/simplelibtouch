using System;
using UIKit;
using CoreGraphics;
using Foundation;
using CoreAnimation;


namespace SimpleLibTouch
{
	public class ActivityOverlayView : UIView
	{
		public ActivityOverlayView(string message, Action cancelDelegate=null) : base(new CGRect(0, 0, 320, 480))
		{
			_cancelDelegate = cancelDelegate;

			// Create the activity indicator
			_activity = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.WhiteLarge);
			AddSubview(_activity);
			
			// Create the message label
			_label = new UILabel(new CGRect(0,0,0,0));
			_label.Text = message;
			_label.Opaque = false;
			_label.BackgroundColor = UIColor.Clear;
			_label.TextColor = UIColor.White;
			_label.Font = UIFont.SystemFontOfSize(22);
			_label.TextAlignment = UITextAlignment.Center;
			AddSubview(_label);
			
			// Create the cancel button
			_button = new CancelButton();
			_button.SetTitle("Cancel", UIControlState.Normal);
			AddSubview(_button);
			
			if (_cancelDelegate==null)
				_button.Hidden = true;
			
			// Setup self
			this.Alpha = 0;
			this.BackgroundColor = UIColor.Black;
			_button.AddTarget(onCancelButtonTapped, UIControlEvent.TouchUpInside);
			
		}
		
		class CancelButton : UIButton
		{
			public CancelButton() : base(CGRect.Empty)
			{
				this.Layer.CornerRadius = 8.0f;
				this.Layer.MasksToBounds = true;
				this.BackgroundColor = UIColor.FromWhiteAlpha(1, 0.3f);
				
				_hotLayer = new CALayer();
				_hotLayer.BackgroundColor = UIColor.FromRGBA(0.5f, 0.5f, 0.5f, 0.75f).CGColor;
				_hotLayer.Hidden = true;
				this.Layer.InsertSublayerAbove(_hotLayer, this.Layer);
			}

			public override bool Highlighted 
			{
				get 
				{
					return base.Highlighted;
				}
				set 
				{
					_hotLayer.Hidden = !value;
					base.Highlighted = value;
				}
			}
			
			public override void LayoutSubviews()
			{
				_hotLayer.Frame = this.Bounds;
				base.LayoutSubviews();
			}
		
			
			CALayer _hotLayer;
		}
		
		Action _cancelDelegate;
		NSObject _observer;
		
		public override void LayoutSubviews()
		{
			nfloat width = Bounds.Width;
			nfloat height = Bounds.Height;
			
			_activity.Frame = new CGRect((width-37)/2, height/2-47, 37,37);
			_label.Frame  = new CGRect(0, height/2 + 10, width, 44);
			_button.Frame = new CGRect(width/2 - 130, height-60, 260, 44);
			base.LayoutSubviews();
		}
		
		void onCancelButtonTapped(object sender, EventArgs args)
		{
			if (_cancelDelegate!=null)
				_cancelDelegate();
		}
		
		public void Show()
		{
			_observer = NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.DidChangeStatusBarOrientationNotification, n=>
			{
				SetupOrientation();
			});
			
			UIApplication.SharedApplication.KeyWindow.AddSubview(this);
			
			SetupOrientation();
			
			_activity.StartAnimating();
			UIView.BeginAnimations(null, IntPtr.Zero);
			UIView.SetAnimationDuration(0.6f);
			this.Alpha = 1f;
			UIView.CommitAnimations();
		}
		
		static object _keepAlive;
		public void Hide()
		{
			_keepAlive = this;
			
			UIView.BeginAnimations(null, IntPtr.Zero);
			UIView.SetAnimationDuration(1);
			this.Alpha = 0;
			UIView.SetAnimationDelegate(this);
			UIView.SetAnimationDidStopSelector(new ObjCRuntime.Selector("transitionDidStop:finished:context:"));
			UIView.CommitAnimations();

			NSNotificationCenter.DefaultCenter.RemoveObserver(_observer);
		}
		

		[Export("transitionDidStop:finished:context:")]
		void transitionDidStop(string animationID, NSNumber finished, IntPtr context)
		{
			_activity.StopAnimating();
			this.RemoveFromSuperview();
			if (_keepAlive == this)
				_keepAlive = null;
		}

		void SetupOrientation()
		{
			var orientation = UIApplication.SharedApplication.StatusBarOrientation;

			float angle = 0;
			
			var newFrame = this.Window.Bounds;
			var statusBarSize = UIApplication.SharedApplication.StatusBarFrame.Size;

			switch (orientation)
			{
				case UIInterfaceOrientation.LandscapeLeft:
					angle = (float)-Math.PI/2.0f;
					newFrame.X += statusBarSize.Width;
					newFrame.Width -= statusBarSize.Width;
					break;
					
				case UIInterfaceOrientation.LandscapeRight:
					angle = (float)Math.PI/2.0f;
					newFrame.Width -= statusBarSize.Width;
					break;
					
				case UIInterfaceOrientation.Portrait:
		            angle = 0.0f;
					newFrame.Y +=statusBarSize.Height;
					newFrame.Height -= statusBarSize.Height;
					break;
					
				case UIInterfaceOrientation.PortraitUpsideDown:
					angle = (float)Math.PI;
					newFrame.Height -= statusBarSize.Height;
					break;
			}
			
			this.Transform = CGAffineTransform.MakeRotation(angle);
			this.Frame = newFrame;
		}
		UILabel _label;
		UIButton _button;
		UIActivityIndicatorView _activity;
	}
}

