using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SimpleLib
{
	public class CsonException : Exception
	{
		public CsonException(string str) : base(string.Format("Error in data - {0}", str))
		{
		}
	}


	public abstract class CsonNode
	{
		public CsonSection Parent
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		protected virtual void OnChanged()
		{
			if (Parent != null)
				Parent.OnChanged();
		}
	}

	public class CsonEntry : CsonNode
	{
		public CsonEntry(string name, object value)
		{
			this.Name = name;
			this.Value = value;
		}

		object _value;
		public object Value
		{
			get
			{
				return _value;
			}
			set
			{
				if (object.Equals(_value, value))
					return;

				_value = value;
				OnChanged();
			}
		}
	}

	public class CsonSection : CsonNode
	{
		public CsonSection(string name=null)
		{
			this.Name = name;
		}
		
		static Dictionary<Type, Dictionary<string, object>> _enumMap;
		static Dictionary<string, object> GetEnumMap(Type type)
		{
			// Make to map of enum types is created
			if (_enumMap==null)
				_enumMap = new Dictionary<Type, Dictionary<string, object>>();
			
			// Find the map of enum values for this enum type
			Dictionary<string, object> map;
			if (!_enumMap.TryGetValue(type, out map))
			{
				map = new Dictionary<string, object>();
				foreach (var v in Enum.GetValues(type))
				{
					map.Add(v.ToString(), v);
				}
				_enumMap.Add(type, map);
			}

			return map;
		}


		public T Get<T>(string key, T def = default(T))
		{
			// Look up value
			CsonEntry entry;
			if (!_values.TryGetValue(key, out entry))
				return def;
			
			if (typeof(T).IsEnum && entry.Value!=null && entry.Value is String)
			{
				var map = GetEnumMap(typeof(T));
				object value;
				if (map.TryGetValue((string)entry.Value, out value))
					return (T)value;
			}
			
			// Try to convert value
			return (T)Convert.ChangeType(entry.Value, typeof(T));
		}

		public bool Contains(string key)
		{
			return _values.ContainsKey(key);
		}

		public IEnumerable<CsonEntry> Values
		{
			get
			{
				return _values.Values;
			}
		}

		public T Get<T>(string section, string key, T def = default(T))
		{
			CsonSection s = FindSection(section);
			if (s == null)
				return def;

			return s.Get<T>(key, def);
		}

		public T Set<T>(string key, T value)
		{
			this[key] = value;
			OnChanged();
			return value;
		}

		public T Set<T>(string section, string key, T value)
		{
			return GetSection(section).Set<T>(key, value);
		}

		public CsonSection FindSection(string name)
		{
			if (_subSections == null)
				return null;
			return _subSections.FirstOrDefault(x => string.Compare(name, x.Name, true) == 0);
		}

		public CsonSection GetSection(string name)
		{
			if (_subSections == null)
				return AddSection(name);

			var s = _subSections.FirstOrDefault(x => string.Compare(name, x.Name, true) == 0);
			return s == null ? AddSection(name) : s;
		}

		public CsonSection AddSection(string name)
		{
			var s = new CsonSection(name);
			AddSection(s);
			return s;
		}

		public object this[string key]
		{
			get
			{
				CsonEntry entry;
				if (!_values.TryGetValue(key, out entry))
					return null;

				return entry.Value;
			}
			set
			{
				CsonEntry entry;
				if (!_values.TryGetValue(key, out entry))
				{
					entry = new CsonEntry(key, value);
					entry.Parent = this;
					_values.Add(key, entry);
				}
				else
				{
					entry.Value = value;
				}
			}
		}

		public IList<CsonEntry> Entries
		{
			get
			{
				return _values.Values.ToList();
			}
		}

		public IList<string> Names
		{
			get
			{
				return _values.Keys.ToList();
			}
		}
		
		public bool ContainsValue(string name)
		{
			return _values.ContainsKey(name);
		}

		public IList<CsonSection> Sections
		{
			get
			{
				if (_subSections == null)
					return new CsonSection[] { };
				return _subSections;
			}
		}

		public void AddSection(CsonSection section)
		{
			InsertSection(_subSections==null ? 0 : _subSections.Count, section);
		}

		public void InsertSection(int position, CsonSection section)
		{
			if (section.Parent != null)
				throw new InvalidOperationException("Section already belongs to another Cson file");

			if (_subSections == null)
				_subSections = new List<CsonSection>();
				
			section.Parent = this;
			_subSections.Add(section);

			OnChanged();
		}

		public void RemoveSection(CsonSection section)
		{
			if (section.Parent != this)
				throw new InvalidOperationException("Section doesn't below to this section");
			section.Parent = null;
			_subSections.Remove(section);
			OnChanged();
		}

		public void RemoveSectionAt(int position)
		{
			if (_subSections == null)
				_subSections = new List<CsonSection>();

			_subSections[position].Parent = null;
			_subSections.RemoveAt(position);
			OnChanged();
		}

		public override string ToString()
		{
			var w = new StringWriter();
			CsonFormat.Write(this, w);
			return w.ToString();
		}

		public void Clear()
		{
			// Reset old content
			_values.Clear();
			_subSections = null;
		}


		Dictionary<string, CsonEntry> _values = new Dictionary<string, CsonEntry>(StringComparer.InvariantCultureIgnoreCase);
		List<CsonSection> _subSections;
	}

	public enum Token
	{
		EOF,
		Operator,
		Identifier,
		Literal,
	}

	public class Tokenizer
	{
		public Tokenizer(TextReader r)
		{
			_reader = r;
			_buf = new char[1024];
			_pos = 0;

			// Load up
			ReadMore();
			NextChar();
			NextToken();
		}

		TextReader _reader;
		char[] _buf;
		int _pos;
		int _bufUsed;
		public char Current;
		StringBuilder _sb = new StringBuilder();

		char NextChar()
		{
			if (_pos >= _bufUsed)
			{
				if (_bufUsed > 0)
				{
					ReadMore();
				}
				if (_bufUsed == 0)
				{
					Current = '\0';
					return Current;
				}
			}

			// Store next character
			return Current = _buf[_pos++];
		}

		public void NextToken()
		{
			_sb.Length=0;
			State state = State.Initial;

			while (true)
			{
				switch (state)
				{
					case State.Initial:
						if (IsIdentifierLeadChar(Current))
						{
							state = State.Identifier;
							break;
						}
						else if (char.IsDigit(Current) || Current=='-')
						{
							TokenizeNumber();
							CurrentToken = Token.Literal;
							return;
						}
						else if (char.IsWhiteSpace(Current))
						{
							NextChar();
							break;
						}

						switch (Current)
						{
							case '/':
								NextChar();
								state = State.Slash;
								break;

							case '\"':
								NextChar();
								_sb.Length = 0;
								state = State.QuotedString;
								break;

							case '\'':
								NextChar();
								_sb.Length = 0;
								state = State.QuotedChar;
								break;

							case '\0':
								CurrentToken = Token.EOF;
								return;

							case '{':
							case '}':
							case '[':
							case ']':
							case '=':
							case ':':
							case ';':
							case ',':
								this.String = new String(Current, 1);
								NextChar();
								CurrentToken = Token.Operator;
								return;

							default:
								throw new CsonException(string.Format("syntax error, unrecognized character '{0}'", Current));
						}
						break;

					case State.Slash:
						switch (Current)
						{
							case '/':
								NextChar();
								state = State.SingleLineComment;
								break;

							case '*':
								NextChar();
								state = State.BlockComment;
								break;
						}

						String = new string(Current, 1);
						NextChar();
						CurrentToken = Token.Operator;
						break;

					case State.SingleLineComment:
						if (Current == '\r' || Current == '\n')
						{
							state = State.Initial;
						}
						NextChar();
						break;

					case State.BlockComment:
						if (Current == '*')
						{
							NextChar();
							if (Current == '/')
							{
								state = State.Initial;
							}
						}
						NextChar();
						break;

					case State.Identifier:
						if (IsIdentifierChar(Current))
						{
							_sb.Append(Current);
							NextChar();
						}
						else
						{
							this.String = _sb.ToString();
							switch (this.String)
							{
								case "true":
									this.Literal = true;
									CurrentToken = Token.Literal;
									break;

								case "false":
									this.Literal = false;
									CurrentToken = Token.Literal;
									break;

								case "null":
									this.Literal = null;
									CurrentToken = Token.Literal;
									break;

								default:
									CurrentToken = Token.Identifier;
									break;
							}
							return;
						}
						break;

					case State.QuotedString:
						if (Current == '\\')
						{
							AppendSpecialChar();
						}
						else if (Current == '\"')
						{
							this.Literal = _sb.ToString();
							CurrentToken = Token.Literal;
							NextChar();
							return;
						}
						else
						{
							_sb.Append(Current);
							NextChar();
							continue;
						}

						break;

					case State.QuotedChar:
						if (Current == '\\')
						{
							AppendSpecialChar();
						}
						else if (Current == '\'')
						{
							if (CSharpMode)
							{
								if (_sb.Length > 1)
									throw new CsonException("too many characters in literal");

								this.Literal = _sb[0];
							}
							else
							{
								this.Literal = _sb.ToString();
							}

							CurrentToken = Token.Literal;
							NextChar();
							return;
						}
						else
						{
							_sb.Append(Current);
							NextChar();
							continue;
						}
						break;
				}
			}
		}

		void TokenizeNumber()
		{
			_sb.Length = 0;

			// Leading -
			if (Current == '-')
			{
				_sb.Append(Current);
				NextChar();
				if (!Char.IsDigit(Current))
				{
					throw new CsonException("Expected digit to follow negative sign");
				}
			}

			// Parse all digits
			bool fp = false;
			while (char.IsDigit(Current) || Current == '.' || Current == 'e' || Current == 'E' || Current=='x' || Current=='X')
			{
				if (Current == 'e' || Current == 'E')
				{
					fp = true;
					_sb.Append(Current);

					NextChar();
					if (Current == '-' || Current == '+')
					{
						_sb.Append(Current);
						NextChar();
					}
				}
				else
				{
					if (Current == '.')
						fp = true;

					_sb.Append(Current);
					NextChar();
				}
			}

			Type type = fp ? typeof(double) : typeof(int);
			if (CSharpMode)
			{
				switch (Current)
				{
					case 'F':
					case 'f':
						NextChar();
						type = typeof(float);
						return;

					case 'd':
					case 'D':
						type = typeof(double);
						NextChar();
						break;

					case 'S':
					case 's':
						NextChar();

						if (Current=='b' || Current=='B')
						{
							type = typeof(sbyte);
							NextChar();
						}
						else
						{
							type = typeof(short);
						}
						break;

					case 'l':
					case 'L':
						type = typeof(long);
						NextChar();
						break;

					case 'U':
					case 'u':
						NextChar();
						switch (Current)
						{
							case 's':
							case 'S':
								type = typeof(ushort);
								NextChar();
								break;

							case 'l':
							case 'L':
								type = typeof(ushort);
								NextChar();
								break;

							default:
								type = typeof(uint);
								break;
						}
						break;

					case 'b':
					case 'B':
						type = typeof(byte);
						NextChar();
						break;

					case 'm':
					case 'M':
						type = typeof(decimal);
						NextChar();
						break;
				}
			}

			if (char.IsLetterOrDigit(Current))
				throw new CsonException(string.Format("invalid character following number '{0}'", Current));

			// Convert type
			try
			{
				this.Literal = Convert.ChangeType(_sb.ToString(), type, System.Globalization.CultureInfo.InvariantCulture);
			}
			catch
			{
				throw new CsonException(string.Format("incorrectly formatted number '{0}'", _sb.ToString()));
			}

		}

		public void Check(Token tokenRequired, string String)
		{
			if (tokenRequired != CurrentToken)
			{
				throw new CsonException(string.Format("syntax error - expected {0} found {1}", tokenRequired, CurrentToken));
			}

			if (String!=null && this.String!=String)
			{
				throw new CsonException(string.Format("syntax error - expected '{0}' found '{1}'", String, this.String));
			}
		}

		public void Skip(Token tokenRequired, string String)
		{
			Check(tokenRequired, String);
			NextToken();
		}


		public bool SkipIf(Token tokenRequired, string String)
		{
			if (tokenRequired == CurrentToken && (String == null || this.String == String))
			{
				NextToken();
				return true;
			}
			return false;
		}


		public bool CSharpMode
		{
			get;
			set;
		}


		void AppendSpecialChar()
		{
			NextChar();
			var escape = Current;
			NextChar();
			switch (escape)
			{
				case '\'': _sb.Append('\''); break;
				case '\"': _sb.Append('\"'); break;
				case '\\': _sb.Append('\\'); break;
				case 'r': _sb.Append('\r'); break;
				case 'n': _sb.Append('\n'); break;
				case 't': _sb.Append('\t'); break;
				case '0': _sb.Append('\0'); break;

				default:
					throw new CsonException(string.Format("Invalid escape sequence in string literal: '\\{0}'", Current));
			}
		}

		public Token CurrentToken
		{
			get;
			private set;
		}

		private enum State
		{
			Initial,
			Slash,
			SingleLineComment,
			BlockComment,
			Identifier,
			QuotedString,
			QuotedChar,
		}

		public string String
		{
			get;
			private set;
		}

		public object Literal
		{
			get;
			private set;
		}

		void ReadMore()
		{
			_bufUsed = _reader.Read(_buf, 0, _buf.Length);
			_pos = 0;
		}

		public void SkipWhitespace()
		{
		}


		public static bool IsIdentifierChar(char ch)
		{
			return Char.IsLetterOrDigit(ch) || ch == '_' || ch == '$';
		}

		public static bool IsIdentifierLeadChar(char ch)
		{
			return Char.IsLetter(ch) || ch == '_' || ch == '$';
		}

		public static bool IsIdentifier(string str)
		{
			return IsIdentifierLeadChar(str[0]) && str.All(x => IsIdentifierChar(x));
		}

	}

	public class CsonFile : CsonSection
	{
		public CsonFile()
		{
		}

		public string Filename
		{
			get;
			set;
		}

		public bool Modified
		{
			get;
			set;
		}

		public Action OnModifiedHandler
		{
			get;
			set;
		}

		public void Load(string filename)
		{
			// Store the file name (if specified)
			if (filename!=null)
				Filename = filename;

			// Clear old content
			Clear();

			// Load new content
			if (System.IO.File.Exists(Filename))
			{
				_loading = true;
				try
				{
					CsonFormat.ParseInto(this, System.IO.File.ReadAllText(Filename));
				}
				finally
				{
					_loading = false;
				}
			}

			Modified = false;
		}

		public void Save(string filename=null)
		{
			// New filename ?
			if (filename == null)
				filename = Filename;

			System.IO.File.WriteAllText(filename, this.ToString());
			Modified = false;
		}

		bool _loading;
		protected override void OnChanged()
		{
			Modified = true;

			if (!_loading && OnModifiedHandler != null)
				OnModifiedHandler();
		}
	}

	public static class CsonFormat
	{
		public static string Format(CsonSection s)
		{
			var w = new StringWriter();
			Write(s, w);
			return w.ToString();
		}

		public static void Write(CsonSection s, TextWriter t)
		{
			Write(s, t, "");
		}

		public static void Write(CsonSection s, TextWriter t, string indent)
		{
			if (s.Name != null)
				t.WriteLine("{0}{1}", indent, QuoteStringUnlessIdentifier(s.Name));

			t.WriteLine("{0}{{", indent);

			// Write values
			foreach (var kvp in s.Values)
			{
				t.WriteLine("{0}\t{1} = {2};", indent, QuoteStringUnlessIdentifier(kvp.Name), FormatValue(kvp.Value));
			}

			// Write subsections
			if (s.Sections != null)
			{
				foreach (var section in s.Sections)
				{
					Write(section, t, indent + "\t");
				}
			}

			t.WriteLine("{0}}}", indent);
		}

		private static string FormatValue(object value)
		{
			// Nulls are allowed!
			if (value == null)
				return "null";

			var t = value.GetType();

			// String?
			if (t == typeof(string))
			{
				return QuoteString((string)value);
			}

			// Boolean/integer?
			if (t == typeof(int) || t == typeof(bool))
			{
				return (string)Convert.ChangeType(value, typeof(string), System.Globalization.CultureInfo.InvariantCulture);
			}

			// Double
			if (t == typeof(double))
			{
				return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:R}D", value);
			}

			// Float
			if (t == typeof(float))
			{
				return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:R}F", value);
			}

			// Unsigned int
			if (t == typeof(uint))
			{
				return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}U", value);
			}

			// Long
			if (t == typeof(long))
			{
				return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}L", value);
			}

			// Unsigned long
			if (t == typeof(ulong))
			{
				return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}UL", value);
			}

			// Short
			if (t == typeof(short))
			{
				return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}S", value);
			}

			// Unsigned short
			if (t == typeof(ushort))
			{
				return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}US", value);
			}

			// Decimal
			if (t == typeof(decimal))
			{
				return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}M", value);
			}

			// Char
			if (t == typeof(char))
			{
				char ch = (char)value;

				// Special cases
				if (ch == '\\')
					return "\\";
				if (ch == '\'')
					return "\'";

				// Ordinary character
				return string.Format(System.Globalization.CultureInfo.InvariantCulture, "'{0}'", value);
			}

			// Byte
			if (t == typeof(byte))
			{
				return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}B", value);
			}

			// Byte array
			if (t == typeof(byte[]))
			{
				return string.Format(System.Globalization.CultureInfo.InvariantCulture, "base64({0})", Convert.ToBase64String((byte[])value));
			}

			// Enum value
			if (t.IsEnum)
			{
				return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}", value.ToString());
			}

			return QuoteString((string)Convert.ChangeType(value, typeof(string), System.Globalization.CultureInfo.InvariantCulture));
		}

		private static string QuoteString(string str)
		{
			var sb = new StringBuilder();
			sb.Append("\"");
			foreach (var ch in str)
			{
				switch (ch)
				{
					case '\"':
						sb.Append("\\\"");
						break;

					case '\'':
						sb.Append("\\\'");
						break;

					case '\r':
						sb.Append("\\r");
						break;

					case '\n':
						sb.Append("\\n");
						break;

					case '\t':
						sb.Append("\\t");
						break;

					case '\0':
						sb.Append("\\0");
						break;

					case '\\':
						sb.Append("\\\\");
						break;

					default:
						sb.Append(ch);
						break;
				}
			}
			sb.Append("\"");
			return sb.ToString();
		}

		private static string QuoteStringUnlessIdentifier(string str)
		{
			return Tokenizer.IsIdentifier(str) ? str : QuoteString(str);
		}

		public static void ParseInto(CsonSection s, string str)
		{
			ParseInto(s, new StringReader(str));
		}

		public static void ParseInto(CsonSection s, TextReader r)
		{
			ParseInto(s, new Tokenizer(r) { CSharpMode = true });
		}
									
		public static void ParseInto(CsonSection s, Tokenizer p)
		{
			if (p.CurrentToken == Token.Identifier)
			{
				s.Name = p.String;
				p.NextToken();
			}
			else if (p.CurrentToken == Token.Literal && p.Literal is String)
			{
				s.Name = (string)p.Literal;
				p.NextToken();
			}

			p.Skip(Token.Operator, "{");
			ParseSection(s, p);
			p.Skip(Token.Operator, "}");


			if (p.CurrentToken!=Token.EOF)
				throw new CsonException(string.Format("unexpected character '{0}'", p.Current));
		}

		static void ParseSection(CsonSection s, Tokenizer p)
		{
			s.Clear();

			while (true)
			{
				string name=null;

				if (p.CurrentToken==Token.Identifier)
				{
					name = p.String;
				}
				else if (p.CurrentToken == Token.Literal && p.Literal is string)
				{
					name = (string)p.Literal;
				}

				// Can't process it - could be end of the section
				if (name == null)
					return;

				p.NextToken();

				if (p.SkipIf(Token.Operator, "="))
				{
					// Check for duplicate keys
					if (s.ContainsValue(name))
						throw new CsonException(string.Format("duplicate value name '{0}'", name));

					if (p.CurrentToken == Token.Identifier)
					{
						s.Set(name, p.String);
					}
					else
					{
						p.Check(Token.Literal, null);
						s.Set(name, p.Literal);
					}

					p.NextToken();
					p.Skip(Token.Operator, ";");
				}
				else if (p.SkipIf(Token.Operator, "{"))
				{
					var s2 = s.AddSection(name);
					ParseSection(s2, p);

					p.Skip(Token.Operator, "}");
				}
				else
				{
					throw new CsonException("expected '=' or '{' following section or value name");
				}
			}
		}
	}

	public static class JsonFormat
	{
		public static string Format(CsonSection s)
		{
			var w = new StringWriter();
			Write(s, w);
			return w.ToString();
		}

		public static void Write(CsonSection s, TextWriter w)
		{
			Write(s, w, "");
		}

		public static void Write(CsonSection s, TextWriter w, string indent)
		{
			w.WriteLine("{0}{{", indent);

			var original_indent = indent;
			indent += "\t";

			// Write values
			bool bNeedComma = false;
			foreach (var v in s.Values)
			{
				if (bNeedComma)
					w.WriteLine(",");
				bNeedComma = true;

				w.Write("{0}\"{1}\": {2}", indent, EscapeString(v.Name), FormatValue(v.Value));
			}

			// Write subsections
			if (s.Sections != null && s.Sections.Count > 0)
			{
				// Build a list of all section names
				foreach (var name in s.Sections.Select(x => x.Name).Distinct())
				{
					if (bNeedComma)
						w.WriteLine(",");
					bNeedComma = true;

					w.WriteLine("{0}\"{1}\":", indent, EscapeString(name));

					var sections = s.Sections.Where(x => x.Name == name).ToList();

					if (sections.Count == 1)
					{
						Write(sections[0], w, indent);
					}
					else
					{
						w.WriteLine("{0}[", indent);
						bool first = true;
						foreach (var s2 in sections)
						{
							if (!first)
								w.WriteLine(",");
							first = false;

							Write(s2, w, indent + "\t");
						}
						if (!first)
							w.WriteLine();
						w.Write("{0}]", indent);
					}
				}
			}

			if (bNeedComma)
				w.WriteLine();

			w.Write("{0}}}", original_indent);
		}


		public static string FormatValue(object value)
		{
			// Nulls are allowed!
			if (value == null)
				return "null";

			var t = value.GetType();

			// String?
			if (t == typeof(string))
			{
				return string.Format(System.Globalization.CultureInfo.InvariantCulture, "\"{0}\"", EscapeString((string)value));
			}

			// Char?
			if (t == typeof(char))
			{
				return string.Format(System.Globalization.CultureInfo.InvariantCulture, "\"{0}\"", EscapeString(new string((char)value, 1)));
			}

			// Boolean
			if (t == typeof(bool))
			{
				return ((bool)value) ? "true" : "false";
			}

			// Numeric
			if (t == typeof(int)
				|| t == typeof(uint)
				|| t == typeof(long)
				|| t == typeof(ulong)
				|| t == typeof(short)
				|| t == typeof(ushort)
				|| t == typeof(decimal)
				|| t == typeof(byte)
				|| t == typeof(sbyte)
				)
			{
				return (string)Convert.ChangeType(value, typeof(string), System.Globalization.CultureInfo.InvariantCulture);
			}

			// Floating point
			if (t == typeof(float))
				return ((float)value).ToString("R", System.Globalization.CultureInfo.InvariantCulture);

			if (t == typeof(double))
				return ((double)value).ToString("R", System.Globalization.CultureInfo.InvariantCulture);

			return string.Format(System.Globalization.CultureInfo.InvariantCulture, "\"{0}\"", EscapeString(value.ToString()));
		}

		private static string EscapeString(string str)
		{
			return 
				str.Replace("\"", "\\\"")
				.Replace("\r", "\\r")
				.Replace("\n", "\\n")
				.Replace("\t", "\\t")
				.Replace("\0", "\\0")
				.Replace("\\", "\\\\")
				.Replace("\'", "\\'");
		}

		public static void ParseInto(CsonSection s, string str)
		{
			ParseInto(s, new StringReader(str));
		}

		public static void ParseInto(CsonSection s, TextReader r)
		{
			var p = new Tokenizer(r);
			ParseInto(s, p);
			if (p.CurrentToken != Token.EOF)
				throw new CsonException(string.Format("unexpected character '{0}'", p.Current));
		}


		static void ParseInto(CsonSection s, Tokenizer p)
		{
			p.Skip(Token.Operator, "{");

			while (true)
			{
				string valueName = null;
				if (p.CurrentToken==Token.Identifier)
				{
					valueName = p.String;
				}
				else if (p.CurrentToken == Token.Literal && p.Literal is String)
				{
					valueName = (string)p.Literal;
				}
				else
				{
					throw new CsonException("syntax erorr, expected string literal or identifier");
				}

				p.NextToken();
				p.Skip(Token.Operator, ":");

				if (p.CurrentToken == Token.Operator && p.String == "{")
				{
					var s2 = s.AddSection(valueName);
					ParseInto(s2, p);
				}
				else if (p.SkipIf(Token.Operator, "["))
				{
					while (true)
					{
						var s2 = s.AddSection(valueName);
						ParseInto(s2, p);
						if (p.SkipIf(Token.Operator, ","))
							continue;
						break;
					}
					p.Skip(Token.Operator, "]");
				}
				else if (p.CurrentToken == Token.Literal)
				{
					// Check for duplicate keys
					if (s.ContainsValue(valueName))
						throw new CsonException(string.Format("duplicate value name '{0}'", valueName));

					s.Set(valueName, p.Literal);
					p.NextToken();
				}

				if (p.SkipIf(Token.Operator, ","))
					continue;
				break;
			}

			p.Skip(Token.Operator, "}");
		}
	}

}

