using System;
using System.Collections.Generic;
using UIKit;
using Foundation;
using ObjCRuntime;
using CoreGraphics;


namespace SimpleLibTouch
{
	public abstract class TableViewController : 
					UITableViewController
	{
		public TableViewController(UITableViewStyle Style) : base(Style)
		{
		}

		public TableViewController(string nibName, NSBundle bundle) : base(nibName, bundle)
		{
			Theme.Instance.ApplyTheme(this.TableView);
			Theme.Instance.ApplyTheme(this);
		}

		public override bool ShouldAutorotateToInterfaceOrientation(UIInterfaceOrientation toInterfaceOrientation)
		{
			return true;
		}

		public override void ViewDidLoad ()
		{
			Theme.Instance.ApplyTheme(this.TableView);
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			Theme.Instance.ApplyTheme(this);
		}

		Dictionary<Type, UINib> _nibLoaders;
		NSObject _nibOwner;
		NSDictionary _nibOptions;

		public T DequeueOrLoadNibCell<T>(NSObject owner=null) where T:UITableViewCell
		{
			// Try to dequeue an existing one
			var cell = (T)TableView.DequeueReusableCell(typeof(T).FullName);
			if (cell!=null)
				return cell;
			
			// Create nib loaders dictionary
			if (_nibLoaders == null)
			{
				_nibLoaders = new Dictionary<Type, UINib>();
			}
			
			// Create a nib loader
			UINib loader;
			if (!_nibLoaders.TryGetValue(typeof(T), out loader))
			{
				loader = UINib.FromName(typeof(T).Name, NSBundle.MainBundle);
				_nibLoaders.Add(typeof(T), loader);
			}
			
			// Use a default owner?
			if (owner==null)
			{
				if (_nibOwner==null)
					_nibOwner = new NSObject();
				owner = _nibOwner;
			}
			
			// We need nib options dictionary
			if (_nibOptions==null)
				_nibOptions = new NSDictionary();
			
			// Finally, create the cell
			cell = (T)loader.Instantiate(owner, _nibOptions)[0];
			if (cell.ReuseIdentifier != typeof(T).FullName)
			{
				throw new InvalidOperationException(string.Format("ReuseIdentifier in Nib ({0}), doesn't match type name ({1})", cell.ReuseIdentifier, typeof(T).FullName));
			}
			return cell;
		}

		public T DequeueOrCreateCell<T>() where T:UITableViewCell, new()
		{
			// Try to dequeue an existing one
			var cell = (T)TableView.DequeueReusableCell(typeof(T).FullName);
			if (cell!=null)
				return cell;

			return new T();
		}
		
	}
}

