using System;
namespace SimpleLibTouch
{
	public class LinkView : HintView
	{
		public LinkView(string Text, bool SmallText = false) : base(Text, SmallText)
		{
			_label.Font = UIFont.SystemFontOfSize(_label.Font.PointSize);
		}
	}
}

