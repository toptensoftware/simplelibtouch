using System;
namespace SimpleLibTouch
{
	public class DefaultTableViewCell : TableViewCell
	{
		public DefaultTableViewCell() : base(UIKit.UITableViewCellStyle.Default)
		{
			if (Theme.Instance!=null)
				Theme.Instance.ApplyTheme(this);
		}
	}
}

