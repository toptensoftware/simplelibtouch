using System;
using Foundation;


namespace SimpleLibTouch
{
	public interface ISelectableCell
	{
		void OnSelected(NSIndexPath indexPath);
	}
	
}

