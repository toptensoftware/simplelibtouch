using System;
using UIKit;


namespace SimpleLibTouch
{
	public class ActionSheet : UIActionSheet
	{
		public ActionSheet(string Title, string CancelButton, string DestructiveButton, params string[] OtherButtons) : 
            base(Title, (IUIActionSheetDelegate)null, CancelButton, DestructiveButton, OtherButtons)
		{
			this.Delegate = new ActionSheetDelegate(this);
		}
		
		public Action<nint> OnClicked;
		
		static ActionSheet _keepAlive;
		
		public void Show()
		{
			ShowInView(UIApplication.SharedApplication.KeyWindow);
		}
		
		class ActionSheetDelegate : UIActionSheetDelegate
		{
			public ActionSheetDelegate(ActionSheet owner)
			{
				_owner = owner;
			}
			
			ActionSheet _owner;
			
			public override void Clicked(UIActionSheet actionSheet, nint buttonIndex)
			{
				if (_owner.OnClicked!=null)
					_owner.OnClicked(buttonIndex);
			}
			
			public override void WillPresent(UIActionSheet actionSheet)
			{
				// Keep alive
				_keepAlive = _owner;
			}
			
			public override void WillDismiss(UIActionSheet actionSheet, nint buttonIndex)
			{
				if (_keepAlive == _owner)
					_keepAlive = null;
			}
			
		}
		
	}
}

