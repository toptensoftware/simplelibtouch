using System;
using UIKit;
using CoreGraphics;


namespace SimpleLibTouch
{
	public class Theme
	{
		public Theme()
		{
		}
		
		public static Theme Instance
		{
			get;
			set;
		}

		public virtual void ApplyTheme(UITableView tableView)
		{
			if (FormBackground != null)
				tableView.BackgroundColor = FormBackground;
		}

		public virtual void ApplyTheme(UIViewController vc)
		{
			if (vc.NavigationController != null)
			{
				if (FormTitleBackground != null)
					vc.NavigationController.NavigationBar.BarTintColor = FormTitleBackground;
			}
		}

		public virtual void ApplyTheme(UITableViewCell tableViewCell)
		{
			TextEntryCell tec = tableViewCell as TextEntryCell;
			if (tec!=null)
			{
				tec.TextLabel.Font = this.LabelFont;
				tec.TextLabel.TextColor = this.LabelColor;
				tec.TextField.Font = ValueFont;
				tec.TextField.TextColor = ValueColor;

				if (tec.HintLabel != null)
				{
					tec.HintLabel.Font = this.HintFont;
					tec.HintLabel.TextColor = this.HintColor;
				}
			}

			LabelledValueCell lvc = tableViewCell as LabelledValueCell;
			if (lvc!=null) 
			{
				lvc.TextLabel.Font = this.LabelFont;
				lvc.TextLabel.TextColor = this.LabelColor;
				lvc.DetailTextLabel.Font = this.ValueFont;
				lvc.DetailTextLabel.TextColor = this.ValueColor;
			}


			DisclosureCell dc = tableViewCell as DisclosureCell;
			if (dc!=null) 
			{
				dc.TextLabel.Font = this.LabelFont;
				dc.TextLabel.TextColor = this.LabelColor;
			}

			SwitchCell swc = tableViewCell as SwitchCell;
			if (swc!=null) 
			{
				swc.TextLabel.Font = this.LabelFont;
				swc.TextLabel.TextColor = this.LabelColor;
			}

			CommandCell cc = tableViewCell as CommandCell;
			if (cc!=null) 
			{
				cc.TextLabel.Font = this.LabelFont;
				cc.TextLabel.TextColor = this.LabelColor;
			}

			CheckedCell cmc = tableViewCell as CheckedCell;
			if (cmc != null)
			{
				cmc.TextLabel.Font = this.ValueFont;
				cmc.TextLabel.TextColor = this.ValueColor;
			}

			DefaultTableViewCell dfc = tableViewCell as DefaultTableViewCell;
			if (dfc!=null)
			{
				dfc.TextLabel.Font = this.ValueFont;
				dfc.TextLabel.TextColor = this.ValueColor;
			}
		}

		public virtual void ApplyTheme(HintView hintView)
		{
			if (PlaceHolderFont != null)
				hintView.Label.Font = PlaceHolderFont;

			hintView.Label.TextColor = UIColor.DarkGray;

		}

		public virtual void ApplyTheme(ActionResultOverlayView view)
		{
			if (DefaultFont != null) 
			{
				view.Label.Font = DefaultFont;
			}
		}

		public virtual UIView CreateTitleView(string title)
		{
			return null;
		}
		
		public virtual void SetCellAccessory(UITableViewCell cell, UITableViewCellAccessory accessory)
		{
			cell.Accessory = accessory;			
		}
		
		public virtual void SetCellEditingAccessory(UITableViewCell cell, UITableViewCellAccessory accessory)
		{
			cell.EditingAccessory = accessory;			
		}
		
		public virtual UIView CreateViewForHeaderTitle(UIView reuseView, string title)
		{
			return null;
		}

		public UIFont DefaultFont;
		public UIFont ValueFont;
		public UIColor ValueColor;
		public UIFont LabelFont;
		public UIColor LabelColor;
		public UIFont PlaceHolderFont;
		public UIColor PlaceHolderColor;
		public UIFont HintFont;
		public UIColor HintColor;
		public UIColor FormBackground;
		public UIColor FormTitleBackground;
	}
}

