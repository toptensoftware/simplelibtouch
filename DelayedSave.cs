using System;
using System.Collections.Generic;
using Foundation;

namespace SimpleLibTouch
{
	public static class DelayedSave
	{
		public static void Register(Action callback)
		{
			if (_callbacks==null)
				_callbacks=new List<Action>();

			if (!_callbacks.Contains(callback))
				_callbacks.Add(callback);
				
			// Reset the timer
			StopTimer();
			StartTimer();
		}

		public static void Revoke(Action callback)
		{
			if (_callbacks==null)
				return;

			int pos = _callbacks.IndexOf(callback);
			if (pos>=0)
				_callbacks.RemoveAt(pos);

			if (_callbacks.Count==0)
				StopTimer();
		}

		public static void SaveNow()
		{
			if (_callbacks!=null)
			{
				var cbs = _callbacks.ToArray();
				_callbacks.Clear();
				foreach (var c in cbs)
				{
					c();
				}
			}
			StopTimer();
		}

		static void StartTimer()
		{
			// Quit if we already have a save pending
			if (_saveTimer!=null)
				return;
			
			_saveTimer = NSTimer.CreateScheduledTimer(5, delegate {
				SaveNow();
				_saveTimer = null;
			});
		}

		static void StopTimer()
		{
			if (_saveTimer!=null)
			{
				_saveTimer.Invalidate();
				_saveTimer = null;
			}
		}


		static List<Action> _callbacks;
		static NSTimer _saveTimer;
	}

}

