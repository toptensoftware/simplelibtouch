using System;
using UIKit;
using Foundation;
using ObjCRuntime;
using System.Text;
using CoreGraphics;


namespace SimpleLibTouch
{
	public static class Utils
	{
		public static T FindSuperViewOfType<T>(this UIView view) where T:UIView
		{
			if (view==null)
				return null;
			
			T x = view as T;
			if (x!=null)
				return x;
			
			return FindSuperViewOfType<T>(view.Superview);
		}
		
		public static T FindControllerOfType<T>(this UIView view) where T:UIViewController
		{
			if (view.NextResponder is T)
				return (T)view.NextResponder;
			if (view.NextResponder is UIView)
				return ((UIView)view.NextResponder).FindControllerOfType<T>();
			return null;
		}
		
		public static UIViewController FindController(this UIView view)
		{
			return view.FindControllerOfType<UIViewController>();
		}
		
		public static void FindAndResignFirstResponder(this UIView view)
		{
			var v = view.FindFirstResponder();
			if (v!=null)
				v.ResignFirstResponder();
		}
		
		public static UIView FindFirstResponder(this UIView view)
		{
			if (view.IsFirstResponder)
				return view;
			
			foreach (var v in view.Subviews)
			{
				var temp = v.FindFirstResponder();
				if (temp!=null)
					return temp;
			}
			
			return null;
		}

		public static void InsertRow(this UITableView table, int Section, int Row, UITableViewRowAnimation animation  = UITableViewRowAnimation.Fade)
		{
			table.InsertRows(new NSIndexPath[] { NSIndexPath.FromRowSection(Row, Section) }, animation);
		}
		
		public static void DeleteRow(this UITableView table, int Section, int Row, UITableViewRowAnimation animation  = UITableViewRowAnimation.Fade)
		{
			table.DeleteRows(new NSIndexPath[] { NSIndexPath.FromRowSection(Row, Section) }, animation);
		}
		
		public static void ReloadRow(this UITableView table, int Section, int Row, UITableViewRowAnimation animation  = UITableViewRowAnimation.Fade)
		{
			table.ReloadRows(new NSIndexPath[] { NSIndexPath.FromRowSection(Row, Section) }, animation);
		}
		
		public static void InsertSection(this UITableView table, int Section, UITableViewRowAnimation animation = UITableViewRowAnimation.Fade)
		{
			table.InsertSections(new NSIndexSet((uint)Section), animation);
		}

		public static void DeleteSection(this UITableView table, int Section, UITableViewRowAnimation animation = UITableViewRowAnimation.Fade)
		{
			table.DeleteSections(new NSIndexSet((uint)Section), animation);
		}
		
		public static void ReloadSection(this UITableView table, int Section, UITableViewRowAnimation animation = UITableViewRowAnimation.Fade)
		{
			table.ReloadSections(new NSIndexSet((uint)Section), animation);
		}

		/*
		public static string Description(this UIView view)
		{	
			return NSString.FromHandle(Messaging.intptr_objc_msgSend(view.Handle, new Selector("description").Handle));
		}
		*/
		
		static float _ver = 0;
		public static float iOSVersion
		{
			get
			{
				if (_ver==0)
				{
					var sb = new StringBuilder();
					bool DotHandled = false;
					foreach (var ch in UIDevice.CurrentDevice.SystemVersion)
					{
						if (char.IsDigit(ch))
							sb.Append(ch);
						if (ch=='.' && !DotHandled)
						{
							sb.Append(ch);
							DotHandled = true;
						}
					}
					
					_ver = float.Parse(sb.ToString(), System.Globalization.CultureInfo.InvariantCulture);
				}
			
				return _ver;
			}
		}
		
		public static string UrlEncode(string str)
		{
			var sb = new StringBuilder();
			var data = Encoding.UTF8.GetBytes(str);
			foreach (var ch in data)
			{
				if ((ch>='0' && ch<='9') || (ch>='a' && ch<='z') || (ch>='A' && ch<='Z'))
					sb.Append((char)ch);
				else
					sb.AppendFormat("%{0:X2}", ch);
			}

			return sb.ToString();
					          
		}
		
		public static void Add(this NSMutableDictionary dict, object key, object value)
		{
			dict.Add(NSObject.FromObject(key), NSObject.FromObject(value));
		}

	}
}

