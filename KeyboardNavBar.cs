using System;
using UIKit;
using CoreGraphics;
using System.Collections.Generic;
using System.Reflection;


namespace SimpleLibTouch
{
	public interface IKeyboardNavBarListener
	{
		void OnNext();
		void OnPrevious();
		void OnDone();
	}

	[Flags]
	public enum KeyboardNavBarButtons
	{
		None = 0,
		Done = 0x01,
		Next = 0x02,
		Previous = 0x04,
		All = 0x07,
		Auto = 0xFF,
	}
	
	public class KeyboardNavBar : UIToolbar
	{
		public KeyboardNavBar(IKeyboardNavBarListener listener, KeyboardNavBarButtons buttons = KeyboardNavBarButtons.All) : base(new CGRect(0,0,320,44))
		{
			_listener = listener;
			
			// Toolbar style
			this.BarStyle = UIBarStyle.Default;

			// Create the toolbar items
			_prevButton = new UIBarButtonItem(UIImage.FromBundle("PrevArrow.png"), UIBarButtonItemStyle.Plain, onPrev);
			_nextButton = new UIBarButtonItem(UIImage.FromBundle("NextArrow.png"), UIBarButtonItemStyle.Plain, onNext);
			_doneButton = new UIBarButtonItem(UIImage.FromBundle("HideKeyboard.png"), UIBarButtonItemStyle.Bordered, onDone);

			_nextButton.TintColor = UIColor.Black;
			_prevButton.TintColor = UIColor.Black;
			_doneButton.TintColor = UIColor.Black;

			_spaceItem = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace,null);

			SetButtons(buttons);
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			_listener = null;
			if (disposing)
			{
				_nextButton.Dispose();
				_prevButton.Dispose();
				_doneButton.Dispose();
			}
		}

		public void SetButtons(KeyboardNavBarButtons buttons)
		{
			_prevButton.Enabled = (buttons & KeyboardNavBarButtons.Previous) != 0;
			_nextButton.Enabled = (buttons & KeyboardNavBarButtons.Next) != 0;

			List<UIBarButtonItem> items = new List<UIBarButtonItem>();

			items.Add(_prevButton);
			items.Add(_nextButton);

			if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone && (buttons & KeyboardNavBarButtons.Done)!=0)
			{
				items.Add(_spaceItem);
				items.Add(_doneButton);
			}
			
			
			SetItems(items.ToArray(), false);
		}
		
		IKeyboardNavBarListener _listener;
		UIBarButtonItem _prevButton;
		UIBarButtonItem _nextButton;
		UIBarButtonItem _doneButton;
		UIBarButtonItem _spaceItem;

		void onNext(object sender, EventArgs args)
		{
			_listener.OnNext();
		}

		void onPrev(object sender, EventArgs args)
		{
			_listener.OnPrevious();
		}

		void onDone(object sender, EventArgs args)
		{
			_listener.OnDone();
		}
	}
	
	
}

