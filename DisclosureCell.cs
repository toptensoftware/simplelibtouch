using System;
using UIKit;
using Foundation;


namespace SimpleLibTouch
{
	public class DisclosureCell : TableViewCell, ISelectableCell
	{
		public DisclosureCell(string Label, Type ControllerType, string Icon=null) : base(UITableViewCellStyle.Default)
		{
			this.ControllerType = ControllerType;
			CommonInit(Label, Icon);
		}
		
		public DisclosureCell(string Label, Action handler, string Icon=null) : base(UITableViewCellStyle.Default)
		{
			this.Handler = handler;
			CommonInit(Label, Icon);
		}
		
		public void CommonInit(string Label, string Icon)
		{
			this.Label = Label;
			this._enabled = true;
			this.TextLabel.TextColor = UIColor.FromRGB(0.25f, 0.38f, 0.52f);
			this.ThemedAccessory = UITableViewCellAccessory.DisclosureIndicator;
			this.SelectionStyle = UITableViewCellSelectionStyle.Blue;
			
			if (Icon!=null)
				this.ImageView.Image = UIImage.FromBundle(Icon);
			
			if (Theme.Instance!=null)
				Theme.Instance.ApplyTheme(this);
		}
		
		public string Label
		{
			get
			{
				return this.TextLabel.Text;
			}
			set
			{
				this.TextLabel.Text = value;
			}
		}

		public Action Handler
		{
			get;
			set;
		}
		
		public Type ControllerType
		{
			get;
			set;
		}
		
		bool _enabled = false;
		UIColor _enabledColor = null;
		public bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				if (_enabled==value)
					return;
				_enabled = value;
				
				if (_enabled)
				{
					this.TextLabel.TextColor = _enabledColor;
					this.ThemedAccessory = UITableViewCellAccessory.DisclosureIndicator;
					this.SelectionStyle = UITableViewCellSelectionStyle.Blue;
				}
				else
				{
					// Save enabled color
					_enabledColor = this.TextLabel.TextColor;
					
					this.TextLabel.TextColor = UIColor.FromRGB(0.5f, 0.5f, 0.5f);
					this.ThemedAccessory = UITableViewCellAccessory.None;
					this.SelectionStyle = UITableViewCellSelectionStyle.None;
				}
			}
		}
				
		public Action onSelected;

		#region ISelectableCell implementation
		void ISelectableCell.OnSelected(NSIndexPath indexPath)
		{
			if (!Enabled)
				return;
			
			ResignOtherFirstResponders();
			
			var tvc = TableViewController;
			if (tvc!=null)
				tvc.TableView.DeselectRow(indexPath, true);
			
			if (Handler!=null)
			{
				Handler();
				return;
			}
			
			if (ControllerType!=null)
			{
				var vc = (UIViewController)Activator.CreateInstance(ControllerType);
				TableViewController.NavigationController.PushViewController(vc, true);
			}
		}
		#endregion
	}
	
}

