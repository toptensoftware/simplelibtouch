using System;
using UIKit;


namespace SimpleLibTouch
{
	public abstract class TableViewCell : UITableViewCell
	{
		public TableViewCell(UITableViewCellStyle style) : base(style, null)
		{
		}
		
		public UITableView TableView
		{
			get
			{
				return this.FindSuperViewOfType<UITableView>();
			}
		}
		
		public ManagedTableViewController TableViewController
		{
			get
			{
				return this.FindControllerOfType<ManagedTableViewController>();
			}
		}
		
		protected void ResignOtherFirstResponders()
		{
			var tv = TableView;
			if (tv!=null)
				tv.FindAndResignFirstResponder();			
		}

		public nfloat CalculatePreferredAlignment()
		{
			this.TextLabel.SizeToFit();
			return this.TextLabel.Frame.Width;
		}

		public void SetAlignment(nfloat value)
		{
			if (_alignment != value)
			{
				_alignment = value;
				SetNeedsLayout();
			}
		}

		protected nfloat _alignment;

		UITableViewCellAccessory _accessory;
		public UITableViewCellAccessory ThemedAccessory 
		{
			get 
			{
				return _accessory;
			}
			set 
			{
				_accessory = value;
				if (Theme.Instance!=null)
					Theme.Instance.SetCellAccessory(this, value);
				else
					this.Accessory = value;
			}
		}
		
	}
	
}

