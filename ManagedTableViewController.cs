using System;
using System.Linq;
using UIKit;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;

namespace SimpleLibTouch
{
	public class ManagedTableViewController  : TableViewController, IKeyboardNavBarListener
	{
		public ManagedTableViewController(string Title, UITableViewStyle style = UITableViewStyle.Grouped) : base(style)
		{
			NavigationItem.Title = Title;
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			if (_navbar != null)
				_navbar.Dispose();

			ResetSections();
		}

		public override void DidMoveToParentViewController(UIViewController parent)
		{
			base.DidMoveToParentViewController(parent);
		}

		KeyboardNavBar _navbar;
		public KeyboardNavBar GetKeyboardNavBar()
		{
			if (_navbar==null)
			{
				_navbar = new KeyboardNavBar(this);
			}
			return _navbar;
		}

		public List<Section> Sections
		{
			get { return _sections; }
		}
		
		public void ResetSections()
		{
			_sections.ForEach(x => x.Dispose());
			_sections.Clear();
		}

		bool _wasTableLoaded = false;
		public void StartReload()
		{
			ResetSections();
			_wasTableLoaded = _tableLoaded;
			_tableLoaded = false;
		}

		public void EndReload()
		{
			_tableLoaded = _wasTableLoaded;
			if (_tableLoaded)
			{
				TableView.ReloadData();
			}
		}
		
		public Section AddSection(Section section)
		{
			// Add to list
			_sections.Add(section);	
			SetNeedsCellAlignment();

			// Add to tableView
			if (_tableLoaded)
				TableView.InsertSection(_sections.Count - 1);

			return section;
		}

		public Section InsertSection(int index, Section section)
		{
			_sections.Insert (index, section);
			if (_tableLoaded)
				TableView.InsertSection(index, UITableViewRowAnimation.Automatic);
			return section;
		}

		public void RemoveSection(Section section)
		{
			var index = _sections.IndexOf(section);
			if (index < 0)
				return;

			// Remove from list
			_sections.RemoveAt(index);
			SetNeedsCellAlignment();

			// Remove from table
			if (_tableLoaded)
				TableView.DeleteSection(index);
		}
		
		List<Section> _sections = new List<Section>();

		public void DeleteCell(UITableViewCell cell, UITableViewRowAnimation animation = UITableViewRowAnimation.Fade)
		{
			// Find it's index
			var indexPath = TableView.IndexPathForCell(cell);
			if (indexPath != null)
			{
				_sections[indexPath.Section].Cells.RemoveAt(indexPath.Row);
				if (_tableLoaded)
					TableView.DeleteRow(indexPath.Section, indexPath.Row, animation);
			}
		}

		bool _alignAllSections;
		bool _needsCellAlignment = true;

		public bool AlignAllSections
		{
			get
			{
				return _alignAllSections;
			}
			set
			{
				_alignAllSections = value;
				SetNeedsCellAlignment();
			}
		}

		void SetNeedsCellAlignment()
		{
			_needsCellAlignment = true;
		}

		void AlignCells()
		{
			if (_alignAllSections)
			{
				nfloat max = 0;
				foreach (var s in _sections)
				{
					nfloat thisAlign = s.CalculatePreferredAlignment();
					if (thisAlign > max)
						max = thisAlign;
				}
				foreach (var s in _sections)
				{
					s.SetAlignment(max);
				}
			}
			else
			{
				foreach (var s in _sections)
				{
					s.AlignCells();
				}
			}

			_needsCellAlignment = false;
		}

		public UIView FindFirstResponder()
		{
			return TableView.FindFirstResponder();
		}

		public void UncheckOthersInSection(UITableViewCell cell)
		{
			var indexPath=TableView.IndexPathForCell(cell);
			if (indexPath.Section < 0 || indexPath.Section>=_sections.Count)
				return;
			
			foreach (var c in _sections[indexPath.Section].Cells)
			{
				if (c!=cell && c is CheckedCell)
				{
					((CheckedCell)c).Checked = false;
				}
					
			}
		}

		bool _tableLoaded = false;
		public override nint NumberOfSections(UITableView tableView)
		{
			_tableLoaded = true;
			return _sections.Count;
		}
		
		public override nint RowsInSection(UITableView tableView, nint section)
		{
			return _sections[(int)section].Cells.Count;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			if (_needsCellAlignment)
				this.AlignCells();


			UITableViewCell cell = _sections[indexPath.Section].Cells[indexPath.Row];
			
			var ci = cell as ICellInit;
			if (ci!=null)
				ci.OnInit(this);
			
			return cell;
		}
		
		public override string TitleForHeader(UITableView tableView, nint section)
		{
			return _sections[(int)section].Title;
		}
		
		public override string TitleForFooter(UITableView tableView, nint section)
		{
			return _sections[(int)section].Footer;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			var cell = TableView.CellAt(indexPath);

			ISelectableCell sc = cell as ISelectableCell;
			if (sc!=null)
			{
				sc.OnSelected(indexPath);
			}
		}

		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			var cell = GetCell(tableView, indexPath);

			var cwh = cell as ICellWithHeight;
			if (cwh != null)
				return cwh.MeasureHeight(tableView);
			else
				return 44;
		}

		public override UIView GetViewForFooter(UITableView tableView, nint section)
		{
			var view = _sections[(int)section].FooterView;
			return view;
		}
		
		public override nfloat GetHeightForFooter(UITableView tableView, nint section)
		{
			var view = _sections[(int)section].FooterView;
			if (view!=null)
			{
				if (view is HintView)
				{
					return ((HintView)view).CalculateHeight(TableView.Frame.Width);
				}
				return view.Frame.Height;
			}
			return base.GetHeightForFooter(tableView, section);
		}

		public override UIView GetViewForHeader(UITableView tableView, nint section)
		{
			var view = _sections[(int)section].HeaderView;
			return view;
		}

		public override nfloat GetHeightForHeader(UITableView tableView, nint section)
		{
			var s = _sections[(int)section];
			if (String.IsNullOrEmpty(s.Title) && s.HeaderView == null)
				return 0;

			if (s.HeaderView!=null)
			{
				if (s.HeaderView is HintView)
				{
					return ((HintView)s.HeaderView).CalculateHeight(TableView.Frame.Width);
				}
				return s.HeaderView.Frame.Height;
			}

			return 40;
			//return section == 0 ? 20 : 40;
		}

		static IEnumerable<UIView> AllSubviewsRecursive(UIView This)
		{
			foreach (var v in This.Subviews)
			{
				yield return v;
				foreach (var sv in AllSubviewsRecursive(v))
					yield return sv;
			}
		}



		IEnumerable<UITextField> GetAllTextFields()
		{
			foreach (var section in _sections)
			{
				foreach (var cell in section.Cells)
				{
					foreach (var ed in AllSubviewsRecursive(cell).OfType<UITextField>())
					{
						if (ed.Alpha > 0 && !ed.Hidden && ed.UserInteractionEnabled)
						{
							yield return ed;		
						}
					}
				}
			}
		}
		
		public NSIndexPath IndexPathOfCell(UITableViewCell cell)
		{
			for (int s =0; s<_sections.Count; s++)
			{
				for (int r=0; r<_sections[s].Cells.Count; r++)
				{
					if (_sections[s].Cells[r]==cell)
						return NSIndexPath.FromRowSection(r, s);
				}
			}
			return null;
		}
		
		public void ScrollToCell(UITableViewCell cell)
		{
			var ip = IndexPathOfCell(cell);
			if (ip!=null)
			{
				TableView.ScrollToRow(ip, UITableViewScrollPosition.None, true);
			}
		}
		
		UITableViewCell FindFirstResponderCell()
		{
			UIView v = FindFirstResponder();
			if (v==null)
				return null;
			
			return v.FindSuperViewOfType<UITableViewCell>();
		}
		
		public KeyboardNavBarButtons DetermineAvailableNavBarButtons(UITextField forTextField)
		{
			var cells = GetAllTextFields().ToList();
			if (cells.Count==0)
				return KeyboardNavBarButtons.None;
			
			var buttons = KeyboardNavBarButtons.Done;
			int index = cells.IndexOf(forTextField);
			if (index>0)
				buttons |= KeyboardNavBarButtons.Previous;
			if (index+1<cells.Count)
				buttons |= KeyboardNavBarButtons.Next;
			
			return buttons;
		}

		#region IKeyboardNavBarListener implementation
		void IKeyboardNavBarListener.OnNext()
		{
			var current = FindFirstResponder() as UITextField;
			if (current == null)
				return;

			var fields = GetAllTextFields().ToList();
			
			int index = fields.IndexOf(current)+1;
			if (index>=0 && index<fields.Count)
			{
				fields[index].BecomeFirstResponder();
				ScrollToCell(fields[index].FindSuperViewOfType<UITableViewCell>());
			}
		}

		void IKeyboardNavBarListener.OnPrevious()
		{
			var current = FindFirstResponder() as UITextField;
			if (current == null)
				return;

			var fields = GetAllTextFields().ToList();
			
			int index = fields.IndexOf(current)-1;
			if (index>=0 && index<fields.Count)
			{
				fields[index].BecomeFirstResponder();
				ScrollToCell(fields[index].FindSuperViewOfType<UITableViewCell>());
			}
		}

		void IKeyboardNavBarListener.OnDone()
		{
			TableView.FindAndResignFirstResponder();
		}
		#endregion
	}
}

