using System;
using System.Collections.Generic;
using UIKit;
using System.Linq;

namespace SimpleLibTouch
{
	public class Section : IDisposable
	{
		public Section(string Title, IEnumerable<UITableViewCell> cells)
		{
			this.Title = Title;
			this.Cells.AddRange(cells);
		}
		
		public Section(string Title, params UITableViewCell[] cells)
		{
			this.Title = Title;
			this.Cells.AddRange(cells);
		}
		
		string _title;
		public string Title
		{
			get
			{
				return _title;
			}
			set
			{
				_title = value;
				if (_title!=null && Theme.Instance!=null)
				{
					HeaderView = Theme.Instance.CreateViewForHeaderTitle(HeaderView, _title);
				}
				else
				{
					HeaderView = null;
				}
			}
		}
		
		public string Footer
		{
			get;
			set;
		}
		
		public UIView FooterView
		{
			get;
			set;
		}
		
		public UIView HeaderView
		{
			get;
			set;
		}
		
		public int CheckedCellIndex
		{
			get
			{
				for (int i=0; i<Cells.Count; i++)
				{
					var tvc = Cells[i] as TableViewCell;
					if (tvc!=null)
					{
						if (tvc.ThemedAccessory == UITableViewCellAccessory.Checkmark)
							return i;
					}
					else
					{
						if (Cells[i].Accessory == UITableViewCellAccessory.Checkmark)
							return i;
					}
				}
				return -1;
			}
			set
			{
				for (int i=0; i<Cells.Count; i++)
				{
					var tvc = Cells[i] as TableViewCell;
					if (tvc!=null)
					{
						tvc.ThemedAccessory = i==value ? UITableViewCellAccessory.Checkmark : UITableViewCellAccessory.None;
					}
					else
					{
						Cells[i].Accessory = i==value ? UITableViewCellAccessory.Checkmark : UITableViewCellAccessory.None;
					}
				}
			}
		}

		public nfloat CalculatePreferredAlignment()
		{
			nfloat max = 0;
			foreach (var c in Cells.OfType<TableViewCell>())
			{
				nfloat align = c.CalculatePreferredAlignment();
				if (align > max)
					max = align;
			}

			return max;
		}

		public void SetAlignment(nfloat align)
		{
			foreach (var c in Cells.OfType<TableViewCell>())
			{
				c.SetAlignment(align);
			}
		}

		public void AlignCells()
		{
			SetAlignment(CalculatePreferredAlignment());
		}


		
		public List<UITableViewCell> Cells = new List<UITableViewCell>();

		#region IDisposable implementation

		public void Dispose()
		{
			Cells.ForEach(x => x.Dispose());
			if (HeaderView != null)
				HeaderView.Dispose();
			if (FooterView != null)
				FooterView.Dispose();
		}

		#endregion
	}
	
}

