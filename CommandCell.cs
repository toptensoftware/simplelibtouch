using System;
using UIKit;
using Foundation;


namespace SimpleLibTouch
{
	public class CommandCell : TableViewCell, ISelectableCell
	{
		public CommandCell(string Label, Type ControllerType, string Icon=null) : base(UITableViewCellStyle.Default)
		{
			this.ControllerType = ControllerType;
			CommonInit(Label, Icon);
		}
		
		public CommandCell(string Label, Action handler, string Icon=null) : base(UITableViewCellStyle.Default)
		{
			this.Handler = handler;
			this._enabled = true;
			CommonInit(Label, Icon);
		}
		
		public void CommonInit(string Label, string Icon)
		{
			this.Label = Label;

			if (Icon!=null)
				this.ImageView.Image = UIImage.FromBundle(Icon);
			
			if (Theme.Instance!=null)
				Theme.Instance.ApplyTheme(this);
		}
		
		public string Label
		{
			get
			{
				return this.TextLabel.Text;
			}
			set
			{
				this.TextLabel.Text = value;
			}
		}
		
		public Action Handler
		{
			get;
			set;
		}
		
		public Type ControllerType
		{
			get;
			set;
		}
		
		bool _enabled = false;
		UIColor _enabledColor = null;
		public bool Enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				if (_enabled==value)
					return;
				_enabled = value;
				
				if (_enabled)
				{
					this.TextLabel.TextColor = _enabledColor;
					this.SelectionStyle = UITableViewCellSelectionStyle.Blue;
				}
				else
				{
					// Save enabled color
					_enabledColor = this.TextLabel.TextColor;
					
					this.TextLabel.TextColor = UIColor.FromRGB(0.5f, 0.5f, 0.5f);
					this.SelectionStyle = UITableViewCellSelectionStyle.None;
				}
			}
		}
				
		public Action onSelected;

		#region ISelectableCell implementation
		void ISelectableCell.OnSelected(NSIndexPath indexPath)
		{
			if (!Enabled)
				return;
			
			ResignOtherFirstResponders();
			
			var tvc = TableViewController;
			if (tvc!=null)
				tvc.TableView.DeselectRow(indexPath, true);
			
			if (Handler!=null)
			{
				Handler();
				return;
			}
			
			if (ControllerType!=null)
			{
				var vc = (UIViewController)Activator.CreateInstance(ControllerType);
				TableViewController.NavigationController.PresentModalViewController(vc, true);
			}
		}
		#endregion
	}
	
}

