using System;
using System.Reflection;
using UIKit;
using CoreGraphics;
using Foundation;

namespace SimpleLibTouch
{
	public static class NumPadButton
	{
		// The one and only num pad extra button
		static UIButton _button;
		static string _caption;
		static UITextField _textField;
		static UIImage _buttonBackground;
		static NSObject _observer;

		public static string NegativeSymbol
		{
			get
			{
				return System.Globalization.CultureInfo.CurrentUICulture.NumberFormat.NegativeSign;
			}
		}

		public static string DecimalSymbol
		{
			get
			{
				return System.Globalization.CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator;
			}
		}
		
		// Create the button and set it's caption
		public static UIButton GetInstance()
		{
			if (_button==null)
			{
				bool IsIOS7 = UIDevice.CurrentDevice.SystemVersion[0] >= '7';

				// Load the button background
				_buttonBackground = UIImage.FromBundle(string.Format("NumPadButtonBackground{0}.png", IsIOS7 ? "_ios7" : ""));
				
				// Setup the button's appearance to match the rest of the keyboard
				_button = new UIButton(new CGRect(0, 163, 105, 53));
				_button.TitleLabel.Font = UIFont.SystemFontOfSize(35);
				_button.SetBackgroundImage(_buttonBackground, UIControlState.Highlighted);

				if (IsIOS7)
				{
					_button.Frame = new CGRect(0, 162.5f, 104.5f, 53);
					_button.SetTitleColor(UIColor.DarkTextColor, UIControlState.Normal);
				}
				else
				{
					_button.SetTitleColor(UIColor.FromRGB(77,84,98), UIControlState.Normal);
					_button.SetTitleColor(UIColor.White, UIControlState.Highlighted);
				}
				
				// Add a handler
				_button.AddTarget((s,a) => 
				{ 
						if (_textField!=null)
						{
							if (Utils.iOSVersion>=5)
							{
								_textField.ReplaceText(_textField.SelectedTextRange, _button.TitleLabel.Text);
							}
							else
							{
								_textField.Text += _button.TitleLabel.Text;
							}
						}

				}, UIControlEvent.TouchUpInside);
			}
			
			return _button;
		}
		
		
		// Recurse through the entire window heirarchy looking for the key plane view
		static UIView FindKeyboardButtonContainer(UIView v)
		{
			string desc = v.Description;
			
			if (desc.StartsWith("<UIKBKeyplaneView"))
				return v;
			
			foreach (var sv in v.Subviews)
			{
				var found = FindKeyboardButtonContainer(sv);
				if (found!=null)
					return found;
			}
			
			return null;
		}
		
		// Add a UIButton to the keyboard window
		static bool AddButtonToKeypad(UIButton button)
		{
			foreach (var w in UIApplication.SharedApplication.Windows)
			{
				var buttonContainer = FindKeyboardButtonContainer(w);
				if (buttonContainer!=null)
				{
					buttonContainer.Superview.AddSubview(button);
					return true;
				}
			}
			
			return false;
		}
		
		
		public static void SelectionChanged(UITextField field)
		{		
			if (_caption==null)
				return;
			if (_textField!=field)
				return;

			if (_caption==(NegativeSymbol + DecimalSymbol) || _caption==(NegativeSymbol + DecimalSymbol))
			{
				if (Utils.iOSVersion >= 5)
				{
					var r = field.GetOffsetFromPosition(field.BeginningOfDocument, field.SelectedTextRange.Start);
					if (r==0)
					{
						_button.SetTitle(NegativeSymbol, UIControlState.Normal);
					}
					else
					{
						_button.SetTitle(DecimalSymbol, UIControlState.Normal);
					}
				}
				else
				{
					if (field.Text.Length==0)
					{
						_button.SetTitle(NegativeSymbol, UIControlState.Normal);
					}
					else
					{
						_button.SetTitle(DecimalSymbol, UIControlState.Normal);
					}
				}
			}
			else
			{
				_button.SetTitle(_caption, UIControlState.Normal);
			}
		}
		
		// Set or remove the extra num pad button
		public static void SetNumPadButton(UITextField field, string caption)
		{
			_textField = field;
			_caption = caption;
			
			if (caption==null)
			{
				// Remove the button
				if (_button!=null && _button.Superview!=null)
					_button.RemoveFromSuperview();

				// Cancel any pending notification
				if (_observer!=null)
				{
					NSNotificationCenter.DefaultCenter.RemoveObserver(_observer);
					_observer = null;
				}
			}
			else
			{
				// Get the button
				var button = GetInstance();
				
				// Already parented to the keyboard?
				if (button.Superview==null)
				{
					// No, try to add it immediately (this will work if the keyboard is already visible)
					if (!AddButtonToKeypad(button) && _observer==null)
					{
						// Add an observer for the keyboard being show
						_observer = NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidShowNotification, notification=>
						{
							// Try again
							AddButtonToKeypad(_button);
							
							// Remove the notification
							NSNotificationCenter.DefaultCenter.RemoveObserver(_observer);
							_observer = null;
						});
					}
				}
				
				SelectionChanged(_textField);
			}
		}
	}
}

