using System;
using UIKit;
using CoreGraphics;


namespace SimpleLibTouch
{
	public class HintView : UIView
	{
		public HintView(string Text, bool SmallText = false) : this(new CGRect(0,0,320,44), Text, SmallText)
		{
		}
		
		public HintView(CGRect frame, string Text, bool SmallText = false) : base(frame)
		{
			_margins = CGRect.FromLTRB(10,10,10,10);
			_background = new UIView();
			_background.Layer.CornerRadius = 8;
			_background.BackgroundColor = UIColor.FromRGBA(0.5f, 0.5f, 0.5f, 0.5f);
			AddSubview(_background);
			
			_label = new UILabel();
			_label.TextColor = UIColor.White;
			_label.BackgroundColor = UIColor.Clear;
			_label.TextAlignment = UITextAlignment.Center;
			_label.Lines = 0;
			_label.LineBreakMode = UILineBreakMode.WordWrap;
			
			if (SmallText)
			{
				_label.Font = UIFont.SystemFontOfSize(14);
			}

			if (Theme.Instance != null)
				Theme.Instance.ApplyTheme (this);

			_background.AddSubview(_label);
			
			this.Text = Text;
		}
		
		CGRect _margins;
		CGRect Margins
		{
			get
			{
				return _margins;
			}
			set
			{
				_margins = value;
				SetNeedsLayout();
			}
		}
		
		protected UILabel _label;
		protected UIView _background;

		public UILabel Label
		{
			get 
			{
				return _label;
			}
		}
		
		public string Text
		{
			get
			{
				return _label.Text;
			}
			set
			{
				_label.Text = value;
			}
		}
		
		public override void LayoutSubviews()
		{
			CGRect r = new CGRect(_margins.Left, _margins.Top, 
			                              Frame.Size.Width - (_margins.Left + _margins.Right), 
			                              Frame.Size.Height - (_margins.Top + _margins.Bottom));
			_background.Frame = r;
			
			r = new CGRect(10, 10, r.Width-20, r.Height-20);
			_label.Frame = r;
		}
		
		public nfloat CalculateHeight(nfloat forWidth)
		{
			CGSize size = _label.Text.StringSize(_label.Font, new CGSize(forWidth - (20 + _margins.Left + _margins.Right), 10000), UILineBreakMode.WordWrap);
			return size.Height + 40;
		}
	}
}

