using System;
using Foundation;
using UIKit;


namespace SimpleLibTouch
{
	public class RadioCell : LabelledValueCell
	{
		public RadioCell(string Label, int Value, string[] Options) : base(Label, null)
		{
			this._options = Options;
			this.Value = Value;
			if (Theme.Instance!=null)
				Theme.Instance.ApplyTheme(this);

			this.ThemedAccessory = UIKit.UITableViewCellAccessory.DisclosureIndicator;
		}
		
		string[] _options;
		public string[] Options
		{
			get
			{
				return _options;
			}
			set
			{
				_options = value;
				ShowValue();
			}
		}
		
		int _value;
		public new int Value
		{
			get
			{
				return _value;
			}
			set
			{
				_value = value;
				ShowValue();
			}
		}
		
		void ShowValue()
		{
			if (Value>=0 && Value<Options.Length)
				base.Value = Options[Value];
			else
				base.Value = null;
		}
		
		public string HintText
		{
			get;
			set;
		}
		
		public Action onValueChanged;

		/*
		#region ISelectableCell implementation

		void ISelectableCell.OnSelected(NSIndexPath indexPath)
		{
			ResignOtherFirstResponders();
			TableView.DeselectRow(indexPath, true);
			TableViewController.NavigationController.PushViewController(new ChooseViewController(this), true);
		}
		#endregion
		*/
		
		class ChooseViewController : TableViewController
		{
			public ChooseViewController(RadioCell owner) : base(UIKit.UITableViewStyle.Grouped)
			{
				_owner = owner;
				_selection = _owner.Value;
				NavigationItem.Title = _owner.Label;
				
				if (!string.IsNullOrEmpty(_owner.HintText))
				{
					_hintView = new HintView(_owner.HintText, true);
				}
				    
			}
			
			RadioCell _owner;
			int _selection;
			HintView _hintView;
			
			#region implemented abstract members of SimpleLibTouch.TableViewController
			public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
			{
				var cell = DequeueOrCreateCell<DefaultTableViewCell>();
				cell.TextLabel.Text = _owner.Options[indexPath.Row];
				cell.ThemedAccessory = indexPath.Row == _selection ? UITableViewCellAccessory.Checkmark : UITableViewCellAccessory.None;
				if (Theme.Instance!=null)
					Theme.Instance.ApplyTheme(cell);
				return cell;
			}

			public override nint RowsInSection(UITableView tableView, nint section)
			{
				return _owner.Options.Length;
			}
			
			public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
			{
				if (_selection>0)
				{
					var oldCell = (TableViewCell)TableView.CellAt(NSIndexPath.FromRowSection(_selection, 0));
					if (oldCell!=null)
						oldCell.ThemedAccessory = UITableViewCellAccessory.None;
				}
				
				((TableViewCell)TableView.CellAt(indexPath)).ThemedAccessory = UITableViewCellAccessory.Checkmark;
				
				_selection = indexPath.Row;
				_owner.Value = _selection;
				if (_owner.onValueChanged!=null)
					_owner.onValueChanged();
				
				NavigationController.PopViewController(true);
			}
			public override UIView GetViewForFooter(UITableView tableView, nint section)
			{
				return _hintView;				
			}
			
			public override nfloat GetHeightForFooter(UITableView tableView, nint section)
			{
				if (_hintView==null)
					return 0;
				return _hintView.CalculateHeight(TableView.Frame.Width);
			}
			#endregion
		}
	}
}

