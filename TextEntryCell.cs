using System;
using UIKit;
using CoreGraphics;
using Foundation;


namespace SimpleLibTouch
{
	public class TextEntryCell : TableViewCell, ISelectableCell, ICellInit, ICellWithHeight
	{
		public TextEntryCell(string Label, string Value, string Placeholder=null) : base(UITableViewCellStyle.Value2)
		{
			this.Label= Label;
			this.SelectionStyle = UITableViewCellSelectionStyle.None;

			this.KeyboardNavBarButtons = KeyboardNavBarButtons.Auto;

			int baseWidth = Utils.iOSVersion < 5 ? 219 : 237;
			if (Label==null)
				_textField = new UITextField(new CGRect(10, 10, baseWidth + 73, 24));
			else
				_textField = new UITextField(new CGRect(83, 10, baseWidth, 24));
			_textField.ClearButtonMode = UITextFieldViewMode.WhileEditing;
			_textField.Text = Value;
			_textField.Placeholder = Placeholder;
			_textField.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
			_textField.Delegate = new Delegate(this);

			
			this.ContentView.AddSubview(_textField);
			
			if (Theme.Instance!=null)
				Theme.Instance.ApplyTheme(this);
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			if (disposing)
			{
				_textField.Dispose();
				if (_customClearButton != null)
					_customClearButton.Dispose();
				if (_hintLabel != null)
					_hintLabel.Dispose();
			}


		}

		public override void LayoutSubviews()
		{
			base.LayoutSubviews();

			if (String.IsNullOrEmpty(TextLabel.Text))
			{
				_textField.Frame = new CGRect(15, 10, this.Bounds.Width - 15, 24);
			}
			else
			{
				TextLabel.SizeToFit();
				TextLabel.Frame = new CGRect(15, 10, TextLabel.Frame.Width, TextLabel.Frame.Height);

				nfloat rightMargin = 0;
				if (_textField.ClearButtonMode == UITextFieldViewMode.Never)
					rightMargin = 10;
				_textField.Frame = new CGRect(20 + _alignment, 10, this.Bounds.Width - _alignment - 20 - rightMargin, 24);
			}

			if (_hintLabel != null)
			{
				_hintLabel.Frame = new CGRect(15, 44, this.Bounds.Width - 15, _hintLabel.Frame.Height);
			}


		}
		
		UITextField _textField;

		public void UpdateCustomClearButton()
		{
			if (CustomClearButton!=null)
			{
				CustomClearButton.Hidden = _textField.Text.Length==0 || !_textField.IsFirstResponder;
			}
		}

		UIButton _customClearButton;
		public UIButton CustomClearButton
		{
			get
			{
				return _customClearButton;
			}
			set
			{
				_customClearButton = value;
				if (value!=null)
					value.Hidden = true;
			}
		}
		
		public string Label
		{
			get
			{
				return this.TextLabel.Text;
			}
			set
			{
				this.TextLabel.Text = value;
			}
		}
		
		public string Value
		{
			get
			{
				return _textField.Text;
			}
			set
			{
				_textField.Text = value;
			}
		}
		
		public string Placeholder
		{
			get
			{
				return _textField.Placeholder;
			}
			set
			{
				_textField.Placeholder = value;
			}
		}

		public UILabel HintLabel
		{
			get
			{
				return _hintLabel;
			}
		}

		UILabel _hintLabel;
		public string HintText
		{
			get
			{
				if (_hintLabel != null)
					return _hintLabel.Text;
				else
					return null;
			}
			set
			{
				if (value != null)
				{
					if (value == null)
						return;

					_hintLabel = new UILabel();
					_hintLabel.Text = value;
					_hintLabel.Lines = 0;
					_hintLabel.LineBreakMode = UILineBreakMode.TailTruncation;
					_hintLabel.SizeToFit();
					AddSubview(_hintLabel);
					SetNeedsLayout();

					if (Theme.Instance != null)
						Theme.Instance.ApplyTheme(this);
				}
				else
				{
					if (_hintLabel == null)
					{
						_hintLabel.RemoveFromSuperview();
						_hintLabel = null;
					}
				}
			}
		}
		
		public UITextField TextField
		{
			get
			{
				return _textField;
			}
		}
		
		public UIKeyboardType KeyboardType
		{
			get
			{
				return _textField.KeyboardType;
			}
			set
			{
				_textField.KeyboardType = value;
			}
		}

		public UITextAutocorrectionType AutocorrectionType
		{
			get
			{
				return _textField.AutocorrectionType;
			}
			set
			{
				_textField.AutocorrectionType = value;
			}
		}
		
		public UITextAutocapitalizationType AutocapitalizationType
		{
			get
			{
				return _textField.AutocapitalizationType;
			}
			set
			{
				_textField.AutocapitalizationType = value;
			}
		}

		public UIReturnKeyType ReturnKeyType
		{
			get
			{
				return _textField.ReturnKeyType;
			}
			set
			{
				_textField.ReturnKeyType = value;
			}
		}


		public string ExtraKeyboardButton
		{
			get;
			set;
		}
		
		public KeyboardNavBarButtons KeyboardNavBarButtons
		{
			get;
			set;
		}

		public Func<bool> ShouldReturn
		{
			get;
			set;
		}

		#region ISelectableCell implementation
		void ISelectableCell.OnSelected(NSIndexPath indexPath)
		{
			_textField.BecomeFirstResponder();
		}
		#endregion

		#region ICellInit implementation
		void ICellInit.OnInit(ManagedTableViewController controller)
		{
			if (KeyboardNavBarButtons!=KeyboardNavBarButtons.None)
			{
				_textField.InputAccessoryView = controller.GetKeyboardNavBar();
			}
		}
		#endregion

		#region ICellWithHeight implementation

		public nfloat MeasureHeight(UITableView tableView)
		{
			if (_hintLabel == null)
				return 44;

			return 44 + _hintLabel.Frame.Height + 5;
		}

		#endregion

		protected virtual bool ShouldChangeCharacters(UITextField textField, NSRange range, string replacementString)
		{
			return true;
		}

		
		class Delegate : UITextFieldDelegate
		{
			public Delegate(TextEntryCell owner)
			{
				_owner = owner;
				_owner.TextField.AddTarget(onChanged, UIControlEvent.EditingChanged);
			}

			TextEntryCell _owner;

			void onChanged(object sender, EventArgs args)
			{
				NumPadButton.SelectionChanged((UITextField)sender);
				_owner.UpdateCustomClearButton();
			}
			
			public override void EditingStarted(UITextField textField)
			{
				// Setup numpad extra button
				if (_owner.KeyboardType==UIKeyboardType.NumberPad && _owner.ExtraKeyboardButton!=null)
					NumPadButton.SetNumPadButton(_owner.TextField, _owner.ExtraKeyboardButton);
				
				// Configure the input view
				if (_owner.KeyboardNavBarButtons!=KeyboardNavBarButtons.None && _owner.TableViewController!=null)
				{
					var navbar = textField.InputAccessoryView as KeyboardNavBar;
					if (navbar!=null)
					{
						var buttons = _owner.KeyboardNavBarButtons;
						if (buttons == KeyboardNavBarButtons.Auto)
						{
							buttons = _owner.TableViewController.DetermineAvailableNavBarButtons(_owner.TextField);
						}
						
						navbar.SetButtons(buttons);
					}
				}
				_owner.UpdateCustomClearButton();
			}
			
			public override void EditingEnded(UITextField textField)
			{
				if (_owner.KeyboardType==UIKeyboardType.NumberPad && _owner.ExtraKeyboardButton!=null)
					NumPadButton.SetNumPadButton(null, null);
				_owner.UpdateCustomClearButton();
			}

			public override bool ShouldReturn(UITextField textField)
			{
				if (_owner.ShouldReturn!=null)
					return _owner.ShouldReturn();
				else
					return true;
			}

			public override bool ShouldChangeCharacters(UITextField textField, NSRange range, string replacementString)
			{
				return _owner.ShouldChangeCharacters(textField, range, replacementString);
			}
		}
	}
	
}

