/* PetaJson v0.1 - A tiny Json engine for loosely typed data.
 * Copyright © 2011 Topten Software.  All Rights Reserved.
 * 
 * Apache License 2.0 - http://www.toptensoftware.com/petajson/license
 * 
 * Define PETAJSON_NO_DYNAMIC to omit support for dynamics and expando
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;

namespace SimpleLib
{
	public class Json
	{
		// TextReader/TextWriter

		public static object Parse(TextReader Reader)
		{
			var Parser = new Json.Parser(Reader);
			var val = Parser.Parse();
			return val;
		}

		public static void Write(TextWriter Writer, object Data, bool Pretty)
		{
			Json.Writer.Write(Writer, Data, Pretty ? 0 : -1);
		}

		// Parse/Format

		public static object Parse(string Input)
		{
			return Parse(new StringReader(Input));
		}

		public static string Format(object Data, bool Pretty)
		{
			var w = new StringWriter();
			Write(w, Data, Pretty);
			return w.ToString();
		}

		// Load/Save

		public static object Load(string FileName)
		{
			return Parse(new StreamReader(FileName));
		}

		public static void Save(string FileName, object Data, bool Pretty)
		{
			Write(new StreamWriter(FileName), Data, Pretty);
		}


		// Dates are supported using Microsoft's convention of "\/Date(milliseconds_since_epoch)\/"
		public static DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
		
		public static T Get<T>(object obj, string key, T def)
		{
			if (obj==null)
				return def;
			
			var dict = obj as Dictionary<string, object>;
			object val;
			if (dict.TryGetValue(key, out val))
				return (T)Convert.ChangeType(val, typeof(T));
			
			return def;
		}
		
		class Writer
		{
			public static void Write(TextWriter tw, object data, int indent)
			{
				if (data == null)
				{
					tw.Write("null");
					return;
				}
	
				switch (Type.GetTypeCode(data.GetType()))
				{
					case TypeCode.DateTime:
						tw.Write(string.Format("\"\\/Date({0})\\/\"", (long)((DateTime)data).ToUniversalTime().Subtract(Json.Epoch).TotalMilliseconds));
						return;
	
					case TypeCode.Boolean:
						tw.Write(((bool)data) ? "true" : "false");
						return;
	
					case TypeCode.DBNull:
						tw.Write("null");
						return;
	
					case TypeCode.String:
						WriteString(tw, (string)data);
						return;
	
					case TypeCode.Char:
						WriteString(tw, data.ToString());
						return;
	
					case TypeCode.Byte:
					case TypeCode.UInt16:
					case TypeCode.UInt32:
					case TypeCode.UInt64:
					case TypeCode.Decimal:
					case TypeCode.Int16:
					case TypeCode.Int32:
					case TypeCode.Int64:
					case TypeCode.SByte:
						tw.Write(data.ToString());
						return;
	
					case TypeCode.Double:
					case TypeCode.Single:
						var str = string.Format("{0:R}", data);
						if (str.IndexOf('.') < 0 && str.IndexOf('E') < 0)		// Ensure something to indicate it's a floating point value
							str += ".0";
						tw.Write(str);
						return;
				}
	
				// Various Dictionary enumerations
				var x = data as IEnumerable<KeyValuePair<string, object>>;		
				if (x != null)
				{
					WriteDictionary(tw, CreateEnumerator(x), indent);
					return;
				}
				var e = data as IEnumerable<DictionaryEntry>;
				if (e != null)
				{
					WriteDictionary(tw, e, indent);
					return;
				}
				var d = data as IDictionary;
				if (d != null)
				{
					WriteDictionary(tw, CreateEnumerator(d), indent);
					return;
				}
	
				// Array
				var a = data as IEnumerable;
				if (a != null)
				{
					WriteArray(tw, a, indent);
					return;
				}
	
				// Class object
				if (data.GetType().IsClass)
				{
					WriteDictionary(tw, EnumObjectProperties(data), indent);
					return;
				}
	
				throw new NotSupportedException(string.Format("PetaJson doesn't support writing objects of type {0}", data.GetType().FullName));
			}
	
			public static void WriteIndent(TextWriter output, int indent)
			{
				if (indent < _tabs.Length)
					output.Write(_tabs, 0, indent);
				else
					output.Write(new String('\t', indent));
			}
			public static char[] _tabs = new char[] { '\t', '\t', '\t', '\t', '\t', '\t', '\t', '\t' };
	
			public static void WriteString(TextWriter tw, string str)
			{
				tw.Write("\"");
				foreach (char ch in str)
				{
					switch (ch)
					{
						case '\"': tw.Write("\\\""); break;
						case '\\': tw.Write("\\\\"); break;
						case '\b': tw.Write("\\b"); break;
						case '\f': tw.Write("\\f"); break;
						case '\n': tw.Write("\\n"); break;
						case '\r': tw.Write("\\r"); break;
						case '\t': tw.Write("\\t"); break;
						default:
							if (ch < 32)
								tw.Write("\\u{0:X4}", (int)ch);
							else
								tw.Write(ch);
							break;
					}
				}
				tw.Write("\"");
			}
	
			public static IEnumerable<DictionaryEntry> CreateEnumerator(IDictionary d)
			{
				foreach (DictionaryEntry de in d)
					yield return de;
			}
	
			public static IEnumerable<DictionaryEntry> CreateEnumerator(IEnumerable<KeyValuePair<string, object>> d)
			{
				foreach (var de in d)
					yield return new DictionaryEntry(de.Key, de.Value);
			}
	
			public static IEnumerable<DictionaryEntry> EnumObjectProperties(object o)
			{
				foreach (var p in o.GetType().GetProperties().Where(x=>x.GetIndexParameters().Length==0 && x.CanRead))
					yield return new DictionaryEntry(p.Name, p.GetValue(o, null));
			}
	
			private static void WriteCollection(TextWriter tw, char chOpen, char chClose, IEnumerable a, int indent, Action<object> writeEntry)
			{
				tw.Write(chOpen);
				bool first = true;
				foreach (object o in a)
				{
					tw.Write(indent==-1 ? (first ? "" : ",") : (first ? "\n" : ",\n"));
					WriteIndent(tw, indent < 0 ? 0 : indent + 1);
					writeEntry(o);
					first = false;
				}
				if (!first && indent>=0)
				{
					tw.Write("\n");
					WriteIndent(tw, indent);
				}
				tw.Write(chClose);
			}
	
			public static void WriteArray(TextWriter tw, IEnumerable a, int indent)
			{
				WriteCollection(tw, '[', ']', a, indent, x => Write(tw, x, indent<0 ? -1 : indent + 1));
			}
	
			public static void WriteDictionary(TextWriter tw, IEnumerable<DictionaryEntry> d, int indent)
			{
				WriteCollection(tw, '{', '}', d, indent, x => {
					Write(tw, ((DictionaryEntry)x).Key.ToString(), 0);
					tw.Write(":");
					Write(tw, ((DictionaryEntry)x).Value, indent<0 ? -1 : indent+1);
				});
			}
		}
	
		class Parser
		{
			public Parser(TextReader r)
			{
				_reader = r;
				_line = _reader.ReadLine();
				_pos = 0;
				_current = GetCurrentCharacter();
			}
	
			public object Parse()
			{
				object o = Next(null, null); 
				SkipWhitespace();
				if (_line != null)
					throw new InvalidOperationException("Error in Json stream, unexpected trailing characters");
				return o;
			}
	
			public object Next(object parent, string name)
			{
				SkipWhitespace();
	
				if (char.IsDigit(_current) || _current == '-')
					return ParseNumber();
	
				if (SkipIf('['))
					return ParseArray(parent, name);
	
				if (SkipIf('{'))
					return ParseDictionary(parent, name);
	
				if (SkipIf('\"'))
				{
					long forwardSlashes, milliseconds;
					var str = ParseString(out forwardSlashes);
					if (forwardSlashes==2 
						&& str.StartsWith("/Date(", StringComparison.InvariantCultureIgnoreCase) 
						&& str.EndsWith(")/")
						&& long.TryParse(str.Substring(6, str.Length - 8), System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out milliseconds))
							return Json.Epoch.AddMilliseconds(milliseconds);
	
					return str;
				}
	
				if (IsIdentifierLeadChar(_current))
				{
					string str = ParseIdentifier();
					switch (str)
					{
						case "true":	return true;
						case "false":	return false;
						case "null":	return null;
					}
					throw new InvalidOperationException(string.Format("Error parsing Json stream, unexpected identifier '{0}'", str));
				}
	
				throw new InvalidOperationException(string.Format("Unexpected character in Json stream - '{0}'", _current));
			}
	
			object ParseArray(object parent, string name)
			{
				var array = new List<object>();
	
				while (_current!=']')
				{
					SkipWhitespace();
					array.Add(Next(array, null));
					SkipWhitespace();
					if (!SkipIf(','))
						break;
				}
				Skip(']');
				
				return array.ToArray();
			}
	
			object ParseDictionary(object parent, string name)
			{
				var dictionary = new Dictionary<string, object>();
				while (_current != '}')
				{
					SkipWhitespace();
					var key = ParseStringOrIdentifier();
					SkipWhitespace();
	
					Skip(':');
	
					SkipWhitespace();
					dictionary.Add(key, Next(dictionary, key));
					SkipWhitespace();
	
					if (!SkipIf(','))
						break;
				}
	
				Skip('}');
				
				return dictionary;
			}
	
			string ParseStringOrIdentifier()
			{
				if (IsIdentifierLeadChar(_current))
					return ParseIdentifier();
	
				Skip('\"');
				long unused;
				return ParseString(out unused);
			}
	
			string ParseIdentifier()
			{
				for (_sb.Length = 0; IsIdentifierChar(_current); ReadChar())
					_sb.Append(_current);
				return _sb.ToString();
			}
	
			bool IsIdentifierChar(char ch)
			{
				return char.IsLetterOrDigit(ch) || (ch == '_') || (ch == '$');
			}
	
			bool IsIdentifierLeadChar(char ch)
			{
				return char.IsLetter(ch) || (ch == '_') || (ch == '$');
			}
	
			string ParseString(out long EscapedForwardSlashes)
			{
				_sb.Length = 0;
				EscapedForwardSlashes = 0;
	
				while (_current!='\"')
				{
					if (SkipIf('\\'))
					{
						switch (_current)
						{
							case '\"': _sb.Append('\"'); ReadChar(); break;
							case '\\': _sb.Append('\\'); ReadChar(); break;
							case '/': _sb.Append("/"); ReadChar(); EscapedForwardSlashes++; break;
							case 'b': _sb.Append('\b'); ReadChar(); break;
							case 'f': _sb.Append('\f'); ReadChar(); break;
							case 'n': _sb.Append('\n'); ReadChar(); break;
							case 'r': _sb.Append('\r'); ReadChar(); break;
							case 't': _sb.Append('\t'); ReadChar(); break;
							case 'u':
								ReadChar();
								var sbHex = new StringBuilder();
								for (int i = 0; i < 4; i++)
								{
									sbHex.Append(_current);
									ReadChar();
								}
								_sb.Append((char)Convert.ToUInt16(sbHex.ToString(), 16));
								break
									;
							default:
								throw new InvalidOperationException(string.Format("Error in Json stream, invalid character in string literal - '{0}'", _current));
						}
					}
					else
					{
						_sb.Append(_current);
						ReadChar();
					}
				}
				Skip('\"');
				return _sb.ToString(); ;
			}
	
			object ParseNumber()
			{
				_sb.Length=0;
	
				// Negative number?
				if (SkipIf('-'))
					_sb.Append("-");
	
				// Whole part
				while (char.IsDigit(_current))
				{
					_sb.Append(_current);
					ReadChar();
				}
	
				// Fractional part
				bool fp = false;
				if (SkipIf('.'))
				{
					_sb.Append('.');
					fp = true;
					while (char.IsDigit(_current))
					{
						_sb.Append(_current);
						ReadChar();
					}
				}
	
				// Exponent part
				if (SkipIf('e') || SkipIf('E'))
				{
					fp = true;
					_sb.Append('E');
					if (_current=='+' || _current=='-')
					{
						_sb.Append(_current);
						ReadChar();
					}
	
					if (!char.IsDigit(_current))
						throw new InvalidOperationException(string.Format("Error in Json stream, expected exponent digit, found '{0}'", _current));
	
					while (char.IsDigit(_current))
					{
						_sb.Append(_current);
						ReadChar();
					}
				}
	
				string number = _sb.ToString();
	
				// Try to parse as an integer type
				long lVal;
				if (!fp && long.TryParse(number, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out lVal))
				{
					if (((int)lVal) == lVal)
						return (int)lVal;
					else
						return lVal;
				}
	
				// Must be a double
				double dblVal;
				if (double.TryParse(number, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out dblVal))
					return dblVal;
	
				throw new InvalidOperationException(string.Format("Error in Json stream, unable to parse number '{0}'", number));
			}
	
	
			char GetCurrentCharacter()
			{
				if (_line == null)
					return '\0';
				return _line[_pos];
			}
	
			char ReadChar()
			{
				if (_line != null)
				{
					_pos++;
					if (_pos >= _line.Length)
					{
						_line = _reader.ReadLine();
						_pos = 0;
					}
				}
				return _current = GetCurrentCharacter();
			}
	
			void SkipWhitespace()
			{
				while (true)
				{
					while (char.IsWhiteSpace(_current))
						ReadChar();
	
					if (SkipIf('/'))
					{
						if (SkipIf('/'))
						{
							// Skip to next line
							_pos = _line.Length;
							ReadChar();
							continue;
						}
						else if (SkipIf('*'))
						{
							while (true)
							{
								if (SkipIf('*') && SkipIf('/'))
									break;
								if (_current == '\0')
									throw new InvalidOperationException("Error in Json stream, unterminated comment");
								ReadChar();
							}
							continue;
						}
						else
							throw new InvalidOperationException("Error in Json stream, unexpected character '/'");
					}
					break;
				}
			}
	
			void Skip(char ch)
			{
				if (_current != ch)
					throw new InvalidOperationException(string.Format("Error in Json stream, expected '{0}', found '{1}'", ch, _current));
				ReadChar();
			}
	
			bool SkipIf(char ch)
			{
				if (_current == ch)
				{
					ReadChar();
					return true;
				}
				return false;
			}
	
			TextReader _reader;
			string _line;
			int _pos;
			char _current;
			StringBuilder _sb = new StringBuilder();
		}
	}
}
