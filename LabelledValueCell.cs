using System;
using UIKit;
using Foundation;


namespace SimpleLibTouch
{
	public class LabelledValueCell : TableViewCell, ISelectableCell
	{
		public LabelledValueCell(string Label, string Value, UITableViewCellStyle style = UITableViewCellStyle.Value2) : base(style)
		{
			this.Label = Label;
			this.Value = Value;
			
			if (Theme.Instance!=null)
				Theme.Instance.ApplyTheme(this);

			this.TextLabel.BackgroundColor = UIColor.Yellow;
			this.DetailTextLabel.BackgroundColor = UIColor.Green;
		}
		
		public string Label
		{
			get
			{
				return this.TextLabel.Text;
			}
			set
			{
				this.TextLabel.Text = value;
			}
		}
		
		public string Value
		{
			get
			{
				return this.DetailTextLabel.Text;
			}
			set
			{
				this.DetailTextLabel.Text = value;
				SetNeedsLayout();
			}
		}

		public override void LayoutSubviews()
		{
			base.LayoutSubviews();

			if (string.IsNullOrEmpty(this.TextLabel.Text))
			{
				this.DetailTextLabel.Frame = new CoreGraphics.CGRect(15, 0, ContentView.Bounds.Width - 15, ContentView.Bounds.Height);
			}
			else
			{
				this.TextLabel.SizeToFit();
				this.DetailTextLabel.SizeToFit();

				this.TextLabel.Frame = new CoreGraphics.CGRect(
					15, 
					0, 
					this.TextLabel.Frame.Width, 
					ContentView.Frame.Height
				);

				nfloat width = ContentView.Frame.Width - this.TextLabel.Frame.Right - 5;
				if (width > this.DetailTextLabel.Frame.Width)
					width = this.DetailTextLabel.Frame.Width;

				this.DetailTextLabel.Frame = new CoreGraphics.CGRect(
					ContentView.Frame.Width - width - (Accessory == UITableViewCellAccessory.None ? 15 : 0),
					0, 
					width,
					ContentView.Frame.Height
				);

			}
		}
		
		public Action onSelected;

		#region ISelectableCell implementation
		void ISelectableCell.OnSelected(NSIndexPath indexPath)
		{
			ResignOtherFirstResponders();

			var tvc = TableViewController;
			if (tvc!=null)
				tvc.TableView.DeselectRow(indexPath, true);

			if (onSelected!=null)
				onSelected();
		}
		#endregion
	}
	
}

