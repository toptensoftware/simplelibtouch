using System;
using SimpleLib;

namespace SimpleLibTouch
{
	public class Settings : CsonFile
	{
		public Settings()
		{
		}
		
		protected override void OnChanged()
		{
			
		}

		
		static Settings _instance;
		public static Settings Instance
		{
			get
			{
				if (_instance==null)
				{
					string location = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
					if (!System.IO.Directory.Exists(location))
						System.IO.Directory.CreateDirectory(location);
					string settingsFile = System.IO.Path.Combine(location, "settings.profile");
	
					_instance = new Settings();
					_instance.Load(settingsFile);
				}
				
				return _instance;
			}
		}
	}
}

