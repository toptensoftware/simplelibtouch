using System;

namespace SimpleLibTouch
{
	class TypedWeakReference<T>
	{
		public TypedWeakReference(T target)
		{
			_wr = new WeakReference(target);
		}

		public T Target
		{
			get { return (T)_wr.Target; }
		}

		WeakReference _wr;
	}

}

