using System;
using UIKit;


namespace SimpleLibTouch
{
	public class AlertView : UIAlertView
	{
		public AlertView(string Title, string Message, string cancelButton, params string[] otherButtons) 
            : base(Title, Message, (IUIAlertViewDelegate)null, cancelButton, otherButtons)
		{
			this.Delegate = new AlertViewDelegate(this);
		}
		
		public Action<nint> OnClicked;

		public new void Show()
		{
			_keepAlive = this;
			base.Show();
		}
		
		static AlertView _keepAlive;
		
		
		class AlertViewDelegate : UIAlertViewDelegate
		{
			public AlertViewDelegate(AlertView owner)
			{
				_owner = owner;
			}
			
			AlertView _owner;
			
			public override void WillPresent(UIAlertView alertView)
			{
				_keepAlive = _owner;
			}
			
			
			public override void Dismissed(UIAlertView alertView, nint buttonIndex)
			{
				if (_keepAlive==_owner)
					_keepAlive = null;
				if (_owner.OnClicked!=null)
				{
					_owner.OnClicked(buttonIndex);
				}
			}
			
		}
	}
}

