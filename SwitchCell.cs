using System;
using UIKit;

namespace SimpleLibTouch
{
	public class SwitchCell : TableViewCell
	{
		public SwitchCell(string title, bool value=false) : base(UITableViewCellStyle.Default)
		{
			_switch = new UISwitch();
			_switch.On = value;

			this.ContentView.AddSubview(_switch);
			this.SelectionStyle = UITableViewCellSelectionStyle.None;

			this.TextLabel.Text = title;

			if (Theme.Instance!=null)
				Theme.Instance.ApplyTheme(this);
		}

		public override void LayoutSubviews()
		{
			base.LayoutSubviews();

			_switch.SizeToFit();
			_switch.Frame = new CoreGraphics.CGRect(ContentView.Frame.Right - _switch.Frame.Width - 10, (ContentView.Frame.Height - _switch.Frame.Height) / 2, _switch.Frame.Width, _switch.Frame.Height);
		}

		UISwitch _switch;

		public UISwitch Switch
		{
			get { return _switch; }
		}


		public bool Enabled
		{
			get { return _switch.Enabled; }
			set
			{
				_switch.Enabled = value;
				this.TextLabel.Alpha = value ? 1 : 0.5f;
			}
		}

	}
}

