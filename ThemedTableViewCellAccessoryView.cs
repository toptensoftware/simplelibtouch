using System;
using UIKit;
using CoreGraphics;


namespace SimpleLibTouch
{
	public class ThemedTableViewCellAccessoryView : UIControl
	{
		public ThemedTableViewCellAccessoryView(UITableViewCellAccessory accessory, UIColor color) : base(new CGRect(0,0,14,20))
		{
			Accessory = accessory;
			AccessoryColor = color;
			HighlightColor = UIColor.White;
		}
		
		public UITableViewCellAccessory Accessory
		{
			get;
			set;
		}
		
		public UIColor AccessoryColor
		{
			get;
			set;
		}
		public UIColor HighlightColor
		{
			get;
			set;
		}
		
		const float R_disclosure = 4.5f;
		const float Rx_checkmark = 2.5f;
		const float Ry_checkmark = 4.5f;

		public override void Draw(CGRect rect)
		{
			nfloat x = this.Bounds.Right;
			nfloat y = (this.Bounds.Left + this.Bounds.Right)/2+3;
			
			var ctx = UIGraphics.GetCurrentContext();
			
			if (Accessory == UITableViewCellAccessory.DisclosureIndicator)
			{
				x-=3.0f;
				ctx.SetLineCap(CGLineCap.Square);
				ctx.SetLineJoin(CGLineJoin.Miter);
				ctx.SetLineWidth(3);
	
				ctx.MoveTo(x-R_disclosure, y-R_disclosure);
				ctx.AddLineToPoint(x,y);
				ctx.AddLineToPoint(x-R_disclosure, y+R_disclosure);
			}
			else
			{
				ctx.SetLineCap(CGLineCap.Round);
				ctx.SetLineJoin(CGLineJoin.Round);
				ctx.SetLineWidth(2);
				
				x-=2;
	
				ctx.MoveTo(x-Rx_checkmark*3+0.5f, y+0.5f);
				ctx.AddLineToPoint(x-Rx_checkmark*2,y+Ry_checkmark);
				ctx.AddLineToPoint(x, y-Ry_checkmark);
			}

			if (this.Highlighted)
			{
				this.HighlightColor.SetStroke();
			}
			else
			{
				this.AccessoryColor.SetStroke();
			}

			ctx.StrokePath();
		}
	}
}

