using System;
using UIKit;
using Foundation;


namespace SimpleLibTouch
{
	public class CheckedCell : TableViewCell, ISelectableCell
	{
		public CheckedCell(string Label, bool Checked) : base(UITableViewCellStyle.Default)
		{
			this.Label = Label;
			this.Checked = Checked;
			if (Theme.Instance!=null)
				Theme.Instance.ApplyTheme(this);
		}
		
		public string Label
		{
			get
			{
				return this.TextLabel.Text;
			}
			set
			{
				this.TextLabel.Text = value;
			}
		}
		
		public bool Checked
		{
			get
			{
				return this.ThemedAccessory == UITableViewCellAccessory.Checkmark;
			}
			set
			{
				this.ThemedAccessory = value ? UITableViewCellAccessory.Checkmark : UITableViewCellAccessory.None;
			}
		}
		
		public Action onSelected;

		#region ISelectableCell implementation
		void ISelectableCell.OnSelected(NSIndexPath indexPath)
		{
			ResignOtherFirstResponders();
			
			var tvc = TableViewController;
			if (tvc!=null)
				tvc.UncheckOthersInSection(this);
			
			this.Checked = true;
			
			TableView.DeselectRow(TableView.IndexPathForCell(this), true);
			
			if (onSelected!=null)
				onSelected();
		}
		#endregion
	}
	
}

